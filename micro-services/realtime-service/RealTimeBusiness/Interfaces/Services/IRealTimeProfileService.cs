﻿using ServiceShared.Interfaces.Services;
using ServiceShared.ViewModels;

namespace RealTimeBusiness.Interfaces.Services
{
    public interface IRealTimeProfileService : IBaseProfileService
    {
        #region Methods

        /// <summary>
        ///     Get profile from request.
        /// </summary>
        /// <returns></returns>
        UserViewModel GetProfile();

        /// <summary>
        ///     Set profile to request.
        /// </summary>
        /// <param name="user"></param>
        void SetProfile(UserViewModel user);

        #endregion
    }
}