﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ClientShared.Models;
using RealTimeDb.Models.Entities;
using RealTimeShared.ViewModels;

namespace RealTimeBusiness.Interfaces.Domains
{
    public interface IRealTimeDomain
    {
        #region Methods

        /// <summary>
        /// Add user device token asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<UserDeviceToken> AddUserDeviceTokenAsync(string deviceId, int userId, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Search user device tokens asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<SearchResult<List<UserDeviceToken>>> SearchUserDeviceTokensAsync(SearchDeviceTokenViewModel model, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Add device to group asynchronously.
        /// </summary>
        /// <returns></returns>
        Task AddSignalrConnectionAsync(int userId, string connectionId, string[] groups,
            CancellationToken cancellationtoken = default(CancellationToken));

        /// <summary>
        /// Delete signalr connection id.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<string[]> DeleteSignalrConnectionAsync(string connectionId,
            CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Get groups by searching client id.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        string[] RetrieveClientGroups(string clientId);

        #endregion
    }
}