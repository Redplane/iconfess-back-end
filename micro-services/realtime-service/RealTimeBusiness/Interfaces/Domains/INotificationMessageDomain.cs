﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ClientShared.Models;
using RealTimeBusiness.Models;
using RealTimeDb.Models.Entities;
using RealTimeShared.ViewModels.NotificationMessage;

namespace RealTimeBusiness.Interfaces.Domains
{
    public interface INotificationMessageDomain
    {
        #region Methods

        /// <summary>
        ///     Add notification message to system.
        /// </summary>
        /// <returns></returns>
        Task<NotificationMessage> AddNotificationMessageAsync<T>(
            AddNotificationMessageModel<T> model,
            CancellationToken cancellationToken = default(CancellationToken),
            bool bIsExpressionSupressed = default(bool));
        
        /// <summary>
        ///     Get notification message using id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<NotificationMessage> GetNotificationMessageUsingId(Guid id,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Mark notification as seen message using id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<NotificationMessage> MarkNotificationMessageAsSeen(Guid id,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Search for notification messages using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResult<IList<NotificationMessage>>> SearchNotificationMessagesAsync(
            SearchNotificationMessageViewModel condition,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}