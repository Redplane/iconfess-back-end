﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ClientShared.Enumerations;
using ClientShared.Models;
using Microsoft.EntityFrameworkCore;
using RealTimeBusiness.Interfaces.Domains;
using RealTimeBusiness.Interfaces.Services;
using RealTimeBusiness.Models;
using RealTimeDb.Interfaces;
using RealTimeDb.Models.Entities;
using RealTimeShared.Resources;
using RealTimeShared.ViewModels.NotificationMessage;
using ServiceShared.Exceptions;
using ServiceShared.Interfaces.Services;

namespace RealTimeBusiness.Domains
{
    public class NotificationMessageDomain : INotificationMessageDomain
    {
        #region Constructor

        public NotificationMessageDomain(IBaseTimeService baseTimeService, IRealTimeUnitOfWork unitOfWork,
            IBaseRelationalDbService relationalDbService,
            IRealTimeProfileService profileService)
        {
            _baseTimeService = baseTimeService;
            _unitOfWork = unitOfWork;
            _relationalDbService = relationalDbService;
            _profileService = profileService;
        }

        #endregion

        #region Properties

        private readonly IBaseTimeService _baseTimeService;

        private readonly IRealTimeUnitOfWork _unitOfWork;

        private readonly IBaseRelationalDbService _relationalDbService;

        private readonly IRealTimeProfileService _profileService;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<NotificationMessage> GetNotificationMessageUsingId(Guid id,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get profile information.
            var profile = _profileService.GetProfile();
            if (profile == null)
                return null;

            var notificationMessages = _unitOfWork.NotificationMessages.Search();
            notificationMessages = notificationMessages.Where(x => x.Id == id && x.OwnerId == profile.Id);
            return notificationMessages.FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        ///     Get
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<NotificationMessage> MarkNotificationMessageAsSeen(Guid id,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var notificationMessage = await GetNotificationMessageUsingId(id, cancellationToken);
            if (notificationMessage == null)
                throw new ApiException(HttpStatusCode.NotFound, RealTimeHttpMessages.NotificationNotFound);

            if (notificationMessage.Status == NotificationStatus.Seen)
                return notificationMessage;

            notificationMessage.Status = NotificationStatus.Seen;
            await _unitOfWork.CommitAsync(cancellationToken);
            return notificationMessage;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<IList<NotificationMessage>>> SearchNotificationMessagesAsync(
            SearchNotificationMessageViewModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var notificationmessages = GetNotificationMessages(condition);
            var loadNotificationMessagesResult = new SearchResult<IList<NotificationMessage>>();
            loadNotificationMessagesResult.Total = await notificationmessages.CountAsync(cancellationToken);
            loadNotificationMessagesResult.Records = await _relationalDbService
                .Paginate(notificationmessages, condition.Pagination).ToListAsync(cancellationToken);

            return loadNotificationMessagesResult;
        }

        /// <summary>
        ///     Search for notification messages using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        protected virtual IQueryable<NotificationMessage> GetNotificationMessages(
            SearchNotificationMessageViewModel condition)
        {
            var notificationMessages = _unitOfWork.NotificationMessages.Search();

            var statuses = condition.Statuses;
            if (statuses != null && statuses.Count > 0)
            {
                statuses = statuses.Where(x => Enum.IsDefined(typeof(NotificationStatus), x)).ToHashSet();
                if (statuses != null && statuses.Count > 0)
                    notificationMessages = notificationMessages.Where(x => statuses.Contains(x.Status));
            }

            var createdTime = condition.CreatedTime;
            if (createdTime != null)
            {
                if (createdTime.From != null)
                    notificationMessages = notificationMessages.Where(x => x.CreatedTime >= createdTime.From);

                if (createdTime.To != null)
                    notificationMessages = notificationMessages.Where(x => x.CreatedTime <= createdTime.To);
            }

            return notificationMessages;
        }

        public Task<NotificationMessage> AddNotificationMessageAsync<T>(AddNotificationMessageModel<T> model,
            CancellationToken cancellationToken = default(CancellationToken), bool bIsExpressionSupressed = false)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}