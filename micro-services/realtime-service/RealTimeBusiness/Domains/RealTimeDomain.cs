﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ClientShared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RealTimeBusiness.Interfaces.Domains;
using RealTimeDb.Interfaces;
using RealTimeDb.Models;
using RealTimeDb.Models.Entities;
using RealTimeShared.Resources;
using RealTimeShared.ViewModels;
using ServiceShared.Exceptions;
using ServiceShared.Interfaces.Services;
using ServiceStack.Redis;

namespace RealTimeBusiness.Domains
{
    public class RealTimeDomain : IRealTimeDomain
    {
        #region Constructor

        /// <summary>
        ///     Initialize domain with injectors.
        /// </summary>
        public RealTimeDomain(IRealTimeUnitOfWork realTimeUnitOfWork,
            IBaseTimeService baseTimeService,
            IBaseRelationalDbService baseRelationalDbService,
            IServiceProvider serviceProvider)
        {
            _realTimeUnitOfWork = realTimeUnitOfWork;
            _baseTimeService = baseTimeService;
            _relationalDbService = baseRelationalDbService;
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Real-time unit of work.
        /// </summary>
        private readonly IRealTimeUnitOfWork _realTimeUnitOfWork;

        /// <summary>
        ///     Base time service.
        /// </summary>
        private readonly IBaseTimeService _baseTimeService;

        /// <summary>
        ///     Relational database service.
        /// </summary>
        private readonly IBaseRelationalDbService _relationalDbService;

        /// <summary>
        ///     Service provider.
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="userId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<UserDeviceToken> AddUserDeviceTokenAsync(string deviceId, int userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find all device token that user has.
            var userDeviceTokens = _realTimeUnitOfWork.UserDeviceTokens.Search();
            userDeviceTokens = userDeviceTokens.Where(x => x.DeviceId == deviceId);

            // Device token is being used by another user.
            var bHasDeviceRegistered = await userDeviceTokens.AnyAsync(x => x.UserId == userId, cancellationToken);

            if (bHasDeviceRegistered)
                throw new ApiException(HttpStatusCode.Conflict, RealTimeHttpMessages.DeviceTokenInUse);

            var userDeviceToken = new UserDeviceToken();
            userDeviceToken.DeviceId = deviceId;
            userDeviceToken.UserId = userId;
            userDeviceToken.CreatedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);

            userDeviceToken = _realTimeUnitOfWork.UserDeviceTokens.Insert(userDeviceToken);
            await _realTimeUnitOfWork.CommitAsync(cancellationToken);

            return userDeviceToken;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<UserDeviceToken>>> SearchUserDeviceTokensAsync(
            SearchDeviceTokenViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get device tokens.
            var deviceTokens = _realTimeUnitOfWork.UserDeviceTokens.Search();

            // Search user devices by using device ids.
            var deviceIds = model.DeviceIds;
            if (deviceIds != null && deviceIds.Count > 0)
            {
                deviceIds = deviceIds.Where(x => !string.IsNullOrWhiteSpace(x)).ToHashSet();
                if (deviceIds != null && deviceIds.Count > 0)
                    deviceTokens = deviceTokens.Where(x => deviceIds.Contains(x.DeviceId));
            }

            // Search user devices by using user ids.
            var userIds = model.UserIds;
            if (userIds != null && userIds.Count > 0)
            {
                userIds = userIds.Where(x => x > 0).ToHashSet();
                if (userIds != null && userIds.Count > 0)
                    deviceTokens = deviceTokens.Where(x => userIds.Contains(x.UserId));
            }

            // Initialize load result.
            var loadUserDeviceTokensResult = new SearchResult<List<UserDeviceToken>>();
            loadUserDeviceTokensResult.Total = await deviceTokens.CountAsync(cancellationToken);

            // Count total records.
            var pagination = model.Pagination;
            if (pagination != null && pagination.Page > 0 && pagination.Records > 0)
                loadUserDeviceTokensResult.Records = await _relationalDbService.Paginate(deviceTokens, pagination)
                    .ToListAsync(cancellationToken);
            else
                loadUserDeviceTokensResult.Records = await deviceTokens.ToListAsync(cancellationToken);

            return loadUserDeviceTokensResult;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public async Task AddDeviceToGroupAsync(string deviceId, string group, int userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var dbContextTransaction = _realTimeUnitOfWork.BeginTransactionScope())
            {
                var userDevices = _realTimeUnitOfWork.UserDeviceTokens.Search();
                userDevices = userDevices.Where(x => (x.DeviceId == deviceId) & (x.UserId == userId));
                _realTimeUnitOfWork.UserDeviceTokens.Remove(userDevices);

                var userRealTimeGroups = _realTimeUnitOfWork.UserRealTimeGroups.Search();
                userRealTimeGroups = userRealTimeGroups.Where(x => x.UserId == userId && x.Group == group);
                _realTimeUnitOfWork.UserRealTimeGroups.Remove(userRealTimeGroups);

                var userDeviceToken = new UserDeviceToken();
                userDeviceToken.DeviceId = deviceId;
                userDeviceToken.UserId = userId;
                _realTimeUnitOfWork.UserDeviceTokens.Insert(userDeviceToken);

                var userRealTimeGroup = new UserRealTimeGroup();
                userRealTimeGroup.Id = new Guid();
                userRealTimeGroup.UserId = userId;
                userRealTimeGroup.Group = group;
                userRealTimeGroup.CreatedTime = 0;
                _realTimeUnitOfWork.UserRealTimeGroups.Insert(userRealTimeGroup);

                await _realTimeUnitOfWork.CommitAsync(cancellationToken);
                dbContextTransaction.Commit();
            }
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<string[]> DeleteSignalrConnectionAsync(string connectionId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find & remove the disconnected connection.
            var signalrConnections = _realTimeUnitOfWork.SignalrConnections.Search();
            signalrConnections = signalrConnections.Where(x => x.ClientId == connectionId);
            _realTimeUnitOfWork.SignalrConnections.Remove(signalrConnections);
            await _realTimeUnitOfWork.CommitAsync(cancellationToken);

            using (var realTimeRedisClient = GetRealTimeRedisClient())
            {
                // Get all groups that client takes part in.
                var groups = realTimeRedisClient.Get<string[]>(connectionId);
                realTimeRedisClient.Remove(connectionId);
                return groups;
            }
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task AddSignalrConnectionAsync(int userId, string connectionId, string[] groups,
            CancellationToken cancellationtoken = default(CancellationToken))
        {
            #region Signalr connection

            // Check whether connection id has been saved to this user.
            var signalrConnections = _realTimeUnitOfWork.SignalrConnections.Search();
            signalrConnections = signalrConnections.Where(x => x.ClientId == connectionId);
            var signalrConnection = signalrConnections.FirstOrDefault();
            if (signalrConnection == null)
            {
                signalrConnection = new SignalrConnection();
                signalrConnection.ClientId = connectionId;
                signalrConnection.LastActivityTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
                signalrConnection.UserId = userId;
                _realTimeUnitOfWork.SignalrConnections.Insert(signalrConnection);
            }
            else
                signalrConnection.UserId = userId;

            #endregion

            #region Add client - groups to redis cache

            // Groups are specified.
            if (groups != null && groups.Length > 0)
            {
                using (var redisClient = GetRealTimeRedisClient())
                {
                    var realTimeGroups = redisClient.Get<string[]>(connectionId);
                    if (realTimeGroups == null)
                        redisClient.Set(connectionId, groups);
                    else
                    {
                        realTimeGroups = realTimeGroups.Union(groups).ToArray();
                        redisClient.Set(connectionId, realTimeGroups);
                    }
                }
            }

            #endregion

            await _realTimeUnitOfWork.CommitAsync(cancellationtoken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public virtual string[] RetrieveClientGroups(string clientId)
        {
            // Get real time redis client.
            var realTimeRedisClient = GetRealTimeRedisClient();
            return realTimeRedisClient.Get<string[]>(clientId);
        }

        /// <summary>
        ///     Get real-time redis client.
        /// </summary>
        /// <returns></returns>
        protected IRedisClient GetRealTimeRedisClient()
        {
            var redisClientsManager = _serviceProvider.GetService<IRedisClientsManager>();
            return redisClientsManager.GetClient();
        }

        #endregion
    }
}