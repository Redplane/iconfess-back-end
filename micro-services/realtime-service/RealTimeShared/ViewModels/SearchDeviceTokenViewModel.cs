﻿using ClientShared.Models;
using System.Collections.Generic;

namespace RealTimeShared.ViewModels
{
    public class SearchDeviceTokenViewModel
    {
        #region Properties

        /// <summary>
        ///     Unique identifier of the device group. This value is returned in the response for a successful create operation,
        ///     and is required for all subsequent operations on the device group
        /// </summary>
        public HashSet<string> DeviceIds { get; set; }

        /// <summary>
        ///     The user-defined name of the device group to create or modify
        /// </summary>
        public HashSet<int> UserIds { get; set; }

        /// <summary>
        ///     Time when device token is created.
        /// </summary>
        public double CreatedTime { get; set; }

        /// <summary>
        /// Pagination instance.
        /// </summary>
        public Pagination Pagination { get; set; }

        #endregion
    }
}