using RealTimeDb.Models;
using RealTimeDb.Models.Entities;
using ServiceShared.Interfaces.Services;

namespace RealTimeDb.Interfaces
{
    public interface IRealTimeUnitOfWork : IBaseUnitOfWork
    {
        #region Properties

        IBaseRepository<UserRealTimeGroup> UserRealTimeGroups { get; }

        IBaseRepository<UserDeviceToken> UserDeviceTokens { get; }

        IBaseRepository<SignalrConnection> SignalrConnections { get; }

        /// <summary>
        /// Notification messages.
        /// </summary>
        IBaseRepository<NotificationMessage> NotificationMessages { get; }

        #endregion
    }
}