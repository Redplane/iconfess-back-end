﻿using Microsoft.EntityFrameworkCore;
using RealTimeDb.Interfaces;
using RealTimeDb.Models;
using RealTimeDb.Models.Entities;
using ServiceShared.Interfaces.Services;
using ServiceShared.Services;

namespace RealTimeDb.Services
{
    public class RealTimeUnitOfWork : BaseUnitOfWork, IRealTimeUnitOfWork
    {
        #region Constructors

        /// <summary>
        ///     Initiate unit of work with database context provided by Entity Framework.
        /// </summary>
        public RealTimeUnitOfWork(DbContext dbContext, IBaseRepository<UserRealTimeGroup> userRealTimeGroups,
            IBaseRepository<UserDeviceToken> userDeviceTokens,
            IBaseRepository<SignalrConnection> signalrConnections, 
            IBaseRepository<NotificationMessage> notificationMessages) : base(dbContext)
        {
            UserRealTimeGroups = userRealTimeGroups;
            UserDeviceTokens = userDeviceTokens;
            SignalrConnections = signalrConnections;
            NotificationMessages = notificationMessages;
        }

        #endregion

        #region Properties

        /// <summary>
        /// User device groups
        /// </summary>
        public IBaseRepository<UserRealTimeGroup> UserRealTimeGroups { get;  }

        /// <summary>
        /// User device tokens.
        /// </summary>
        public IBaseRepository<UserDeviceToken> UserDeviceTokens { get;  }

        /// <summary>
        /// Signalr connections.
        /// </summary>
        public IBaseRepository<SignalrConnection> SignalrConnections { get; }

        /// <summary>
        /// Notification messages.
        /// </summary>
        public IBaseRepository<NotificationMessage> NotificationMessages { get; }

        #endregion
    }
}