﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RealTimeDb.Models.Entities;

namespace RealTimeDb.Models.Contexts
{
    public class InMemoryRelationalDbContext : RelationalDbContext
    {
        #region Constructor

        /// <summary>
        ///     Initialize in-memory relational database context with injectors.
        /// </summary>
        /// <param name="dbContextOptions"></param>
        public InMemoryRelationalDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Call the base function.
            base.OnModelCreating(modelBuilder);
            
            AddUserRealTimeGroup(modelBuilder);
        }
        
        /// <summary>
        ///     Add user real-time group.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddUserRealTimeGroup(ModelBuilder modelBuilder)
        {
            var userRealTimeGroups = new List<UserRealTimeGroup>();
            userRealTimeGroups.Add(new UserRealTimeGroup(Guid.NewGuid(), "Admin", 1, 0));
            modelBuilder.Entity<UserRealTimeGroup>().HasData(userRealTimeGroups.ToArray());
        }
        
        #endregion
    }
}