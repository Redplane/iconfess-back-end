﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using RealTimeDb.Models.Entities;

namespace RealTimeDb.Models.Contexts
{
    public class RelationalDbContext : DbContext
    {
        #region Constructors

        /// <summary>
        ///     Initiate database context with connection string.
        /// </summary>
        public RelationalDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {
        }

        #endregion

        #region Properties
        
        /// <summary>
        ///     Signalr connection.
        /// </summary>
        public virtual DbSet<SignalrConnection> SignalrConnections { get; set; }

        /// <summary>
        ///     User real time groups.
        /// </summary>
        public virtual DbSet<UserRealTimeGroup> UserRealTimeGroups { get; set; }

        /// <summary>
        ///     List of device groups.
        /// </summary>
        public virtual DbSet<UserDeviceToken> UserDeviceTokens { get; set; }

        /// <summary>
        ///     List of notification messages in database.
        /// </summary>
        public virtual DbSet<NotificationMessage> NotificationMessages { get; set; }


        #endregion

        #region Methods

        /// <summary>
        ///     Save changes into database.
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            return SaveChanges();
        }

        /// <summary>
        ///     Save changes into database asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<int> CommitAsync()
        {
            return await SaveChangesAsync();
        }

        /// <summary>
        ///     Callback which is fired when model is being created.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Add signalr connection table.
            AddSignalrConnectionTable(modelBuilder);

            // Add signalr connection group table.
            AddUserDeviceGroupTable(modelBuilder);
            
            // Add cloud messaging device group table.
            AddCloudMessagingDeviceGroupTable(modelBuilder);

            AddNotificationMessageTable(modelBuilder);

            // Use model builder to specify composite primary keys.
            // Composite primary keys configuration

            // This is for remove pluralization naming convention in database defined by Entity Framework.
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
                entity.Relational().TableName = entity.DisplayName();

            // Disable cascade delete.
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
        }

        #region Tables initialization
        
        /// <summary>
        ///     Add signalr connection table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddSignalrConnectionTable(ModelBuilder modelBuilder)
        {
            var signalrConnection = modelBuilder.Entity<SignalrConnection>();
            signalrConnection.HasKey(x => x.ClientId);

            signalrConnection.Property(x => x.ClientId).IsRequired();
        }
        
        /// <summary>
        ///     Add signalrl connection group table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddUserDeviceGroupTable(ModelBuilder modelBuilder)
        {
            //var signalrConnectionGroup = modelBuilder.Entity<SignalrConnectionGroup>();
            //signalrConnectionGroup.HasKey(x => x.Id);
            var userDeviceGroup = modelBuilder.Entity<UserRealTimeGroup>();
            userDeviceGroup.HasKey(x => x.Id);
            userDeviceGroup.Property(x => x.Group).IsRequired();
        }

        /// <summary>
        ///     Add cloud messaging device group table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddCloudMessagingDeviceGroupTable(ModelBuilder modelBuilder)
        {
            var userDeviceToken = modelBuilder.Entity<UserDeviceToken>();
            userDeviceToken.HasKey(x => x.DeviceId);
            userDeviceToken.Property(x => x.DeviceId).IsRequired();
        }

        /// <summary>
        ///     Initialize notification message table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddNotificationMessageTable(ModelBuilder modelBuilder)
        {
            // Find notification message.
            var notificationMessage = modelBuilder.Entity<NotificationMessage>();

            // Primary key initialization.
            notificationMessage.HasKey(x => x.Id);
            notificationMessage.Property(x => x.Id).ValueGeneratedOnAdd();
            notificationMessage.Property(x => x.Message).IsRequired();
        }

        #endregion

        #endregion
    }
}