﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using RealTimeBusiness.Interfaces.Services;
using RealTimeMicroService.Interfaces.Services;
using ServiceShared.Authentications.ActionFilters;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.AuthenticationRequirements.Handlers
{
    public class SolidUserRequirementHandler : AuthorizationHandler<SolidUserRequirement>
    {
        #region Constructor

        /// <summary>
        ///     Initiate requirement handler with injectors.
        /// </summary>
        public SolidUserRequirementHandler(
            IRealTimeProfileService profileService, IRealTimeProfileCacheService appProfileCacheService,
            IHttpContextAccessor httpContextAccessor)
        {
            _profileService = profileService;
            _httpContextAccessor = httpContextAccessor;
            _appProfileCacheService = appProfileCacheService;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Handle requirement asychronously.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            SolidUserRequirement requirement)
        {
            // Convert authorization filter context into authorization filter context.
            var authorizationFilterContext = (AuthorizationFilterContext) context.Resource;

            // Get user access token.
            var requestAccessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            try
            {
                // Get access token from cache.
                //var account = _profileCacheService.Read(iId);
                var accessToken = await _appProfileCacheService.FindAccessTokenByCodeAsync(requestAccessToken);

                if (accessToken == null)
                    throw new Exception("Cannot find user information from cache service");

                // Get user information.
                var user = JsonConvert.DeserializeObject<UserViewModel>(accessToken.User);
                if (user == null)
                    throw new Exception("No user information is found in this token.");
                
                _profileService.SetProfile(user);
                context.Succeed(requirement);
            }
            catch
            {
                if (authorizationFilterContext == null)
                {
                    context.Fail();
                    return;
                }

                // Method or controller authorization can be by passed.
                if (authorizationFilterContext.Filters.Any(x => x is ByPassAuthorizationAttribute))
                {
                    _profileService.BypassAuthorizationFilter(context, requirement);
                    return;
                }
                
                context.Fail();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Provides functions to access service which handles identity businesses.
        /// </summary>
        private readonly IRealTimeProfileService _profileService;

        /// <summary>
        /// Real-time profile cache service.
        /// </summary>
        private readonly IRealTimeProfileCacheService _appProfileCacheService;

        /// <summary>
        ///     Context accessor.
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        #endregion
    }
}