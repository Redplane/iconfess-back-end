﻿using ServiceShared.Interfaces.Services;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.Interfaces.Services
{
    /// <summary>
    ///     Service which is for caching connection id with user instances.
    /// </summary>
    public interface IRealTimeConnectionCacheService : IBaseKeyValueCacheService<string, UserViewModel>
    {
    }
}