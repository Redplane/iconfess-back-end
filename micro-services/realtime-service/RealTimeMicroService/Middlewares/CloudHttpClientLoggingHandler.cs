﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace RealTimeMicroService.Middlewares
{
    public class CloudHttpClientLoggingHandler : DelegatingHandler
    {
        #region Properties

        private ILogger<CloudHttpClientLoggingHandler> _logger;

        #endregion

        #region Constructor

        public CloudHttpClientLoggingHandler(ILogger<CloudHttpClientLoggingHandler> logger)
        {
            _logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Invoke send method
        /// </summary>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var httpContent = request.Content;
            if (httpContent == null)
                return base.SendAsync(request, cancellationToken);

            var body = httpContent.ReadAsStringAsync().Result;
            _logger.LogInformation(JsonConvert.SerializeObject(body));

            return base.SendAsync(request, cancellationToken);
        }

        #endregion
    }
}