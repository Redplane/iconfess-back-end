﻿using AutoMapper;
using ServiceShared.Models;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.Models
{
    public class MappingProfile : Profile
    {
        #region Constructor

        /// <summary>
        ///     Initialize automapper mapping profile.
        /// </summary>
        public MappingProfile()
        {
            // Post mapping.
            //CreateMap<AddPostViewModel, Post>();
            CreateMap<InternalMqMessage, InternalMqMessage>();
        }

        #endregion
    }
}