﻿namespace RealTimeMicroService.Models.PushNotification
{
    public class FcmOption
    {
        #region Properties

        /// <summary>
        ///     DeviceId which is used for submitting request to FCM server.
        /// </summary>
        public string ServerKey { get; set; }

        /// <summary>
        ///     Id of project.
        /// </summary>
        public string SenderId { get; set; }

        #endregion
    }
}