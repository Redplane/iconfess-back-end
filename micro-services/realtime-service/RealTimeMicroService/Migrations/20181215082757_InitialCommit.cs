﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RealTimeMicroService.Migrations
{
    public partial class InitialCommit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SignalrConnection",
                columns: table => new
                {
                    ClientId = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    LastActivityTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignalrConnection", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "UserDeviceToken",
                columns: table => new
                {
                    DeviceId = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedTime = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDeviceToken", x => x.DeviceId);
                });

            migrationBuilder.CreateTable(
                name: "UserRealTimeGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Group = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedTime = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRealTimeGroup", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SignalrConnection");

            migrationBuilder.DropTable(
                name: "UserDeviceToken");

            migrationBuilder.DropTable(
                name: "UserRealTimeGroup");
        }
    }
}
