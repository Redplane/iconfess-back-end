﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using RealTimeBusiness.Interfaces.Services;
using RealTimeMicroService.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.Services;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.Services
{
    public class RealTimeProfileService : BaseProfileService, IRealTimeProfileService
    {
        #region Constructor

        public RealTimeProfileService(IOptions<AppJwtModel> appJwt, IHttpContextAccessor httpContextAccessor) : base(
            appJwt,
            httpContextAccessor)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual UserViewModel GetProfile()
        {
            return GetProfile<UserViewModel>();
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="user"></param>
        public virtual void SetProfile(UserViewModel user)
        {
            SetProfile<UserViewModel>(user);
        }

        #endregion
    }
}