﻿using RealTimeMicroService.Interfaces.Services;
using ServiceShared.Services;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.Services
{
    public class RealTimeConnectionCacheService : BaseKeyValueCacheService<string, UserViewModel>,
        IRealTimeConnectionCacheService
    {
        #region Methods

        /// <summary>
        ///     All keys should be lower cased.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public override string FindKey(string key)
        {
            return key.ToLower();
        }

        #endregion
    }
}