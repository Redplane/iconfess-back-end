﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.SwaggerGeneration.Processors.Security;
using RealTimeBusiness.Domains;
using RealTimeBusiness.Interfaces.Domains;
using RealTimeBusiness.Interfaces.Services;
using RealTimeDb.Interfaces;
using RealTimeDb.Models.Contexts;
using RealTimeDb.Services;
using RealTimeMicroService.AuthenticationRequirements;
using RealTimeMicroService.AuthenticationRequirements.Handlers;
using RealTimeMicroService.Configs;
using RealTimeMicroService.Constants;
using RealTimeMicroService.HostedServices;
using RealTimeMicroService.Hubs;
using RealTimeMicroService.Interfaces.Services;
using RealTimeMicroService.Models;
using RealTimeMicroService.Services;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.Services;

namespace RealTimeMicroService
{
    public class Startup
    {
        #region Properties

        /// <summary>
        ///     Application configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when application starts.
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Options registration.
            services.Configure<Dictionary<string, PusherClientOptionModel>>(Configuration.GetSection(RealTimeConfigKeyConstant.PusherRealTimeChannel));

            // Add services to application.
            AddServices(services);

            // Cors configuration.
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.WithExposedHeaders("WWW-Authenticate");
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowCredentials();

            // Add cors configuration to service configuration.
            services.AddCors(options => { options.AddPolicy("AllowAll", corsBuilder.Build()); });

            // Add automaper configuration.
            services.AddAutoMapper(options => options.AddProfile(typeof(MappingProfile)));

            services.AddOptions();

            // Initialize signalr policy.
            services.AddAuthorization(x => x.AddPolicy(RealTimePolicyConstant.DefaultSignalRPolicyName, builder =>
            {
                builder.RequireAuthenticatedUser()
                    .AddRequirements(new SolidUserRequirement());
            }));


            // Add signalr service.
            services.AddSignalR(options => { options.EnableDetailedErrors = true; });

            // Register dependencies injections.
            // Construct mvc options.
            services.AddMvc(mvcOptions =>
                {
                    ////only allow authenticated users
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
#if !ALLOW_ANONYMOUS
                        .AddRequirements(new SolidUserRequirement())
#endif
                        .Build();

                    mvcOptions.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            // Use JWT Bearer authentication in the system.
            app.UseAuthentication();

            // Enable cors.
            app.UseCors("AllowAll");

            // Use signalr connection.
            app.UseSignalR(x => { x.MapHub<NotificationHub>("/hub/notification"); });

            app.UseMvc();
        }

        /// <summary>
        ///     Add dependency injection of services to app.
        /// </summary>
        protected void AddServices(IServiceCollection services)
        {
            // Add entity framework to services collection.
            var sqlConnection = "";

#if USE_SQLITE
            sqlConnection = Configuration.GetConnectionString(AppConfigKeyConstant.SqliteConnectionString);
            services.AddDbContext<RelationalDbContext>(
                options => options.UseSqlite(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_SQLITE_INMEMORY
            sqlConnection = Configuration.GetConnectionString(MainConfigKeyConstant.SqliteConnectionString);
            //services.AddDbContext<RelationalDbContext>(
            //    options => options.UseSqlite(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
            //services.AddScoped<DbContext, RelationalDbContext>();

            var sandboxConnection = new SqliteConnection("Data Source=:memory:");
            sandboxConnection.Open();

            using (var physicalConnection = new SqliteConnection(sqlConnection))
            {
                physicalConnection.Open();
                physicalConnection.BackupDatabase(sandboxConnection);
            }

            services.AddDbContext<RelationalDbContext>(
                options => options.UseSqlite(sandboxConnection, b => b.MigrationsAssembly(nameof(MainMicroService)))
                    .EnableSensitiveDataLogging());
#elif USE_AZURE_SQL
            sqlConnection = Configuration.GetConnectionString("azureSqlServerConnectionString");
            services.AddDbContext<RelationalDatabaseContext>(
                options => options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_IN_MEMORY
            services.AddDbContext<InMemoryRelationalDbContext>(
                options => options.UseInMemoryDatabase("iConfess")
                    .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning)));

            var bDbCreated = false;
            services.AddScoped<DbContext>(context =>
            {
                var inMemoryContext = context.GetService<InMemoryRelationalDbContext>();
                if (!bDbCreated)
                {
                    inMemoryContext.Database.EnsureCreated();
                    bDbCreated = true;
                }
                return context.GetService<InMemoryRelationalDbContext>();
            });
#else
            sqlConnection = Configuration.GetConnectionString("sqlServerConnectionString");
            services.AddDbContext<RelationalDbContext>(options =>
                options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(RealTimeMicroService))));

#endif

            // Add scoped.
            services.AddScoped<DbContext, RelationalDbContext>();

            // Injections configuration.
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IRealTimeUnitOfWork, RealTimeUnitOfWork>();
            services.AddScoped<IBaseRelationalDbService, BaseRelationalDbService>();

            services.AddScoped<IBaseEncryptionService, EncryptionService>();
            services.AddScoped<IRealTimeProfileService, RealTimeProfileService>();
            services.AddScoped<IBaseTimeService, BaseTimeService>();
            services.AddScoped<IRealTimeProfileCacheService, RealTimeProfileCacheService>();
            services.AddScoped<ICloudMessagingService, FcmService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Requirement handler.
            services.AddScoped<IAuthorizationHandler, SolidUserRequirementHandler>();
            services.AddScoped<IRealTimeService, RealTimeService>();

            // Domain registration.
            services.AddScoped<IRealTimeDomain, RealTimeDomain>();

            // Register jwt service.
            JsonWebTokenConfigs.Register(Configuration, services);

            // Config cache client.
            RealTimeCacheClientConfigs.Register(Configuration, services);
            
            // Register HttpClientConfig.
            HttpClientConfigs.Register(Configuration, services);

            // Add hosted service.
            services.AddHostedService<MainMessageHostedService>();

            // Add Swagger API document.
            services.AddSwaggerDocument(settings =>
            {
                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                settings.SerializerSettings = jsonSerializerSettings;

                settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("API Key"));
                settings.DocumentProcessors.Add(new SecurityDefinitionAppender("API Key",
                    new SwaggerSecurityScheme
                    {
                        Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        Description = "Copy 'Bearer ' + valid JWT token into field",
                        In = SwaggerSecurityApiKeyLocation.Header
                    }));
            });
        }

        #endregion
    }
}