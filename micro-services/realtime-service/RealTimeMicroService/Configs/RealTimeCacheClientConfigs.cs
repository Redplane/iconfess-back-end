﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RealTimeMicroService.Constants;
using RealTimeMicroService.Constants.RealTime;
using ServiceShared.Interfaces;
using ServiceShared.Models.CachedEntities;
using ServiceShared.Services;
using ServiceStack.OrmLite;
using ServiceStack.Redis;

namespace RealTimeMicroService.Configs
{
    public class RealTimeCacheClientConfigs
    {

        #region Methods

        /// <summary>
        ///     Register redis configuration for caching.
        /// </summary>
        public static void Register(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            // Get redis configuration.
            var cacheClients = new Dictionary<string, string>();
            configuration
                .GetSection(RealTimeConfigKeyConstant.CacheClients)
                .Bind(cacheClients);

            var accessTokenCacheInstance = new OrmLiteConnectionFactory(cacheClients[RealTimeClientCacheConstant.AccessTokenCacheKeyConstant], SqlServer2017Dialect.Provider);
            var accessTokenDbConnection = new AccessTokenDbConnection(accessTokenCacheInstance);
            accessTokenDbConnection.Open();
            accessTokenDbConnection.CreateTableIfNotExists<AccessToken>();

            // Connect to redis client.
            serviceCollection.AddScoped<IRedisClientsManager>(c =>
                new RedisManagerPool(cacheClients[RealTimeClientCacheConstant.RealTimeClientCacheKeyConstant]));

            // Add redis client manager cache service.
            serviceCollection.AddSingleton<IAccessTokenDbConnection, AccessTokenDbConnection>(x => accessTokenDbConnection);
        }

        #endregion

    }
}