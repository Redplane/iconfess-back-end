﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RealTimeMicroService.Constants;
using RealTimeMicroService.Middlewares;
using RealTimeMicroService.Models.PushNotification;

namespace RealTimeMicroService.Configs
{
    public class HttpClientConfigs
    {
        #region Methods

        /// <summary>
        /// Register configuration.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Register(IConfiguration configuration, IServiceCollection services)
        {
            var cloudMessagingOptions = new FcmOption();
            configuration
                .GetSection(RealTimeConfigKeyConstant.FirebaseCloudMessaging)
                .Bind(cloudMessagingOptions);
            
            // Add HttpClient factory.
            services.AddHttpClient(HttpClientGroupConstant.FcmService, x =>
            {
                x.DefaultRequestHeaders.TryAddWithoutValidation("Authorization",
                    $"key={cloudMessagingOptions.ServerKey}");
                x.DefaultRequestHeaders.TryAddWithoutValidation("project_id", cloudMessagingOptions.SenderId);
            });
            //.AddHttpMessageHandler<CloudHttpClientLoggingHandler>();
        }

        #endregion
    }
}