﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RealTimeBusiness.Interfaces.Domains;
using RealTimeBusiness.Interfaces.Services;
using RealTimeMicroService.Interfaces.Services;
using RealTimeMicroService.Models.PushNotification;
using RealTimeShared.Resources;
using RealTimeShared.ViewModels;
using RealTimeShared.ViewModels.RealTime;
using ServiceShared.Authentications.ActionFilters;
using ServiceShared.Interfaces.Services;

namespace RealTimeMicroService.Controllers
{
    [Route("api/real-time")]
    public class RealTimeController : Controller
    {
        #region Constructor

        /// <summary>
        ///     Initialize controller.
        /// </summary>
        public RealTimeController(IRealTimeProfileService realTimeProfileService,
            IRealTimeDomain realTimeDomain,
            IBaseTimeService baseTimeService,
            IRealTimeService realTimeService,
            ICloudMessagingService cloudMessagingService)
        {
            _realTimeProfileService = realTimeProfileService;
            _realTimeService = realTimeService;
            _cloudMessagingService = cloudMessagingService;
            _realTimeDomain = realTimeDomain;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Instance to manage identity
        /// </summary>
        private readonly IRealTimeProfileService _realTimeProfileService;

        /// <summary>
        ///     Real-time service.
        /// </summary>
        private readonly IRealTimeService _realTimeService;

        /// <summary>
        ///     Cloud messaging service.
        /// </summary>
        private readonly ICloudMessagingService _cloudMessagingService;

        /// <summary>
        /// Domain that handles real-time connection.
        /// </summary>
        private readonly IRealTimeDomain _realTimeDomain;

        #endregion

        #region Methods

        /// <summary>
        ///     Asssign user to push channel which attached to him/her before.
        /// </summary>
        /// <returns></returns>
        [HttpPost("register-device-token")]
        public async Task<IActionResult> AssignPushChannel([FromBody] AssignPushChannelViewModel model)
        {
            #region Model validation

            if (model == null)
            {
                model = new AssignPushChannelViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Get token information.
            var clientIdToken = await _cloudMessagingService.GetCloudMessagingTokenInformationAsync(model.DeviceId);
            if (clientIdToken == null)
            {
                ModelState.AddModelError($"{nameof(model)}.{nameof(model.DeviceId)}",
                    RealTimeHttpMessages.DeviceIdInvalid);
                return BadRequest(ModelState);
            }

            #endregion

            // Get user identity.
            var profile = _realTimeProfileService.GetProfile();

            // Add user device token asynchronously.
            var userDeviceToken = await _realTimeDomain.AddUserDeviceTokenAsync(model.DeviceId, profile.Id);

            return Ok(userDeviceToken);
        }

#if DEBUG

        /// <summary>
        ///     Send to client.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("send-to-clients")]
        [ByPassAuthorization]
        public async Task<IActionResult> SendMessageToSignalrClients(
            [FromBody] SendMessageToSignalrClientViewModel model)
        {
            if (model == null)
            {
                model = new SendMessageToSignalrClientViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _realTimeService.SendRealTimeMessageToClientsAsync(model.ClientIds, model.EventName, model.Message,
                CancellationToken.None);
            return Ok();
        }

        /// <summary>
        ///     Send push notification user group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("push-to-groups")]
        [ByPassAuthorization]
        public async Task<IActionResult> PushToGroup([FromBody] FcmMessage<Dictionary<string, object>> model)
        {
            if (model == null)
            {
                model = new FcmMessage<Dictionary<string, object>>();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _cloudMessagingService.SendAsync(model, CancellationToken.None);
            return Ok();
        }

        /// <summary>
        ///     Send to group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("send-to-groups")]
        [ByPassAuthorization]
        public async Task<IActionResult> SendMessageToSignalrGroups([FromBody] SendMessageToSignalGroupViewModel model)
        {
            if (model == null)
            {
                model = new SendMessageToSignalGroupViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _realTimeService.SendRealTimeMessageToGroupsAsync(model.Groups, model.EventName, model.Message,
                CancellationToken.None);
            return Ok();
        }

        /// <summary>
        ///     Get all registered device token.
        /// </summary>
        /// <returns></returns>
        [HttpPost("device-tokens/search")]
        [ByPassAuthorization]
        public async Task<IActionResult> GetDeviceTokens([FromBody] SearchDeviceTokenViewModel model)
        {
            if (model == null)
            {
                model = new SearchDeviceTokenViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return Ok(ModelState);

            var loadDeviceTokensAsync = await _realTimeDomain.SearchUserDeviceTokensAsync(model);
            return Ok(loadDeviceTokensAsync);
        }

        /// <summary>
        ///     Add device to specific group.
        /// </summary>
        /// <returns></returns>
        [HttpPost("add-device-to-group")]
        public async Task<IActionResult> AddDeviceToGroup([FromBody] AddDeviceToGroupViewModel model)
        {
            if (model == null)
            {
                model = new AddDeviceToGroupViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Get requester profile.
            var profile = _realTimeProfileService.GetProfile();
            await _realTimeDomain.AddUserDeviceTokenAsync(model.DeviceId, profile.Id);

            return Ok();
        }
#endif

        #endregion
    }
}