﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using RealTimeBusiness.Interfaces.Domains;
using RealTimeBusiness.Interfaces.Services;
using RealTimeMicroService.Constants;
using RealTimeMicroService.Interfaces.Services;

namespace RealTimeMicroService.Hubs
{
    [Authorize(RealTimePolicyConstant.DefaultSignalRPolicyName)]
    public class NotificationHub : Hub
    {
        #region Constructor

        /// <summary>
        ///     Initialize hub with injectors.
        /// </summary>
        public NotificationHub(IRealTimeProfileService realTimeProfileService,
            IRealTimeService realTimeService,
            IRealTimeDomain realTimeDomain)
        {
            _realTimeProfileService = realTimeProfileService;
            _realTimeDomain = realTimeDomain;
            _realTimeService = realTimeService;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Real-time profile service.
        /// </summary>
        private readonly IRealTimeProfileService _realTimeProfileService;

        /// <summary>
        ///     Real-time unit of work.
        /// </summary>
        private readonly IRealTimeDomain _realTimeDomain;

        /// <summary>
        /// Real-time service.
        /// </summary>
        private readonly IRealTimeService _realTimeService;

        #endregion

        #region Methods

        /// <summary>
        ///     Called when a client connects to hub.
        /// </summary>
        /// <returns></returns>
        public override Task OnConnectedAsync()
        {
            // Get connection id.
            var connectionId = Context.ConnectionId;
            Debug.WriteLine($"Client {connectionId} has connected to {nameof(NotificationHub)}");
            
            // Get profle
            var profile = _realTimeProfileService.GetProfile();

            // Get user available groups.
            var availableGroups = _realTimeService.GetUserAvailableRealTimeGroups(profile);

            _realTimeDomain.AddSignalrConnectionAsync(profile.Id, connectionId, availableGroups)
                .Wait();

            return base.OnConnectedAsync();
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public override Task OnDisconnectedAsync(Exception exception)
        {
            // Get connection id.
            var connectionId = Context.ConnectionId;

            // Delete real-time connection.
            var groups = _realTimeDomain.DeleteSignalrConnectionAsync(connectionId, CancellationToken.None).Result;

            // Delete connection id from joined groups.
            var pDeleteGroupConnectionIdTasks = new List<Task>();
            foreach (var group in groups)
                pDeleteGroupConnectionIdTasks.Add(Groups.RemoveFromGroupAsync(Context.ConnectionId, group));

            // Wait for all tasks to complete.
            Task.WhenAll(pDeleteGroupConnectionIdTasks)
                .Wait();

            return base.OnDisconnectedAsync(exception);
        }

        #endregion
    }
}