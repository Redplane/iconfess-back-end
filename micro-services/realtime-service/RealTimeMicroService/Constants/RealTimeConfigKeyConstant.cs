﻿namespace RealTimeMicroService.Constants
{
    public class RealTimeConfigKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Redis clients.
        /// </summary>
        public const string CacheClients = "realTimeCacheClients";

        /// <summary>
        /// App json web token
        /// </summary>
        public const string AppJwt = "appJwt";

        /// <summary>
        /// Firebase cloud messaging.
        /// </summary>
        public const string FirebaseCloudMessaging = "firebaseCloudMessaging";

        /// <summary>
        /// Pusher real-time channel.
        /// </summary>
        public const string PusherRealTimeChannel = "pusherRealTimeChannels";
        
        #endregion
    }
}