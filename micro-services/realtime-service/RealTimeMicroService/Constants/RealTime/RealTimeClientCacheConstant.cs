﻿namespace RealTimeMicroService.Constants.RealTime
{
    public class RealTimeClientCacheConstant
    {
        #region  Properties

        /// <summary>
        /// Real-time client cache key constant.
        /// </summary>
        public const string RealTimeClientCacheKeyConstant = "realTimeClientCache";

        /// <summary>
        /// Access token sql cache database.
        /// </summary>
        public const string AccessTokenCacheKeyConstant = "accessTokenSqlCacheClient";


        #endregion
    }
}