﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ClientShared.Enumerations;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using PusherClient;
using RealTimeDb.Interfaces;
using RealTimeDb.Models.Entities;
using RealTimeMicroService.Hubs;
using RealTimeMicroService.Interfaces.Services;
using RealTimeMicroService.Models.PushNotification;
using RealTimeMicroService.Models.PushNotification.Notification;
using ServiceShared.Constants;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.ViewModels;

namespace RealTimeMicroService.HostedServices
{
    public class MainMessageHostedService : IHostedService
    {
        #region Constructor

        /// <summary>
        ///     Initialize hosted service.
        /// </summary>
        public MainMessageHostedService(
            IOptions<Dictionary<string, PusherClientOptionModel>> pusherRealTimeChannelOptions,
            ILogger<MainMessageHostedService> mainMessageHostedServiceLogger,
            IHubContext<NotificationHub> notificationHubContext,
            IServiceProvider serviceProvider)
        {
            var pusherRealTimeChannels = pusherRealTimeChannelOptions.Value;
            var mainRealTimeMqChannel = pusherRealTimeChannels[MqClientConstants.MesasgeBroadcaster];
            var pusherClientOptions = new PusherOptions();
            pusherClientOptions.Cluster = mainRealTimeMqChannel.Cluster;

            // Client initialization.
            _mainMessagePusherClient = new Pusher(mainRealTimeMqChannel.Key, pusherClientOptions);

            // Property binding.
            _mainMessageHostedServiceLogger = mainMessageHostedServiceLogger;

            // Context binding.
            _notificationHubContext = notificationHubContext;

            // Property binding.
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Client whic is used for receiving / broadcasting messages between main - real-time service.
        /// </summary>
        private readonly Pusher _mainMessagePusherClient;

        private readonly ILogger<MainMessageHostedService> _mainMessageHostedServiceLogger;

        /// <summary>
        ///     Context to access to notification hub.
        /// </summary>
        private readonly IHubContext<NotificationHub> _notificationHubContext;

        /// <summary>
        ///     Provider to resolve services.
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            // Start connection to pusher server.
            _mainMessagePusherClient.Connected += MainMessagePusherClientOnConnected;
            _mainMessagePusherClient.Error += MainMessagePusherClientOnError;
            _mainMessagePusherClient.Connect();
            return Task.CompletedTask;
        }


        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task StopAsync(CancellationToken cancellationToken)
        {
            // Disconnect pusher client.
            _mainMessagePusherClient.Disconnect();

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Called when main message pusher channel connection has been established.
        /// </summary>
        /// <param name="sender"></param>
        private void MainMessagePusherClientOnConnected(object sender)
        {
            _mainMessageHostedServiceLogger.LogInformation("Connection from real-time -> main has been established.");
            var mainServiceChannelSubscription =
                _mainMessagePusherClient.Subscribe(MqChannelNameConstants.MainServiceChannelName);
            mainServiceChannelSubscription.Subscribed += MainServiceChannelSubscriptionOnSubscribed;
        }

        /// <summary>
        ///     Called when main message pusher channel connection has error.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="error"></param>
        private void MainMessagePusherClientOnError(object sender, PusherException error)
        {
            _mainMessageHostedServiceLogger.LogError(error.Message);
        }

        /// <summary>
        ///     Called when main channel subscribed.
        /// </summary>
        /// <param name="sender"></param>
        protected void MainServiceChannelSubscriptionOnSubscribed(object sender)
        {
            _mainMessageHostedServiceLogger.LogInformation(
                $"Channel {MqChannelNameConstants.MainServiceChannelName} has been subscribed");
            _mainMessagePusherClient.Bind(MqEventNameConstants.SendMessageFromMainToRealTime, async data =>
            {
                var jObject = (JObject)data;
                var mqMessage = jObject.ToObject<InternalMqMessage>();

                // Get unit of work instance.
                var serviceScope = _serviceProvider.CreateScope();
                var unitOfWork = serviceScope.ServiceProvider.GetService<IRealTimeUnitOfWork>();
                var realTimeService = serviceScope.ServiceProvider.GetService<IRealTimeService>();
                var timeService = serviceScope.ServiceProvider.GetService<IBaseTimeService>();
                var cloudMessagingService = serviceScope.ServiceProvider.GetService<ICloudMessagingService>();

                // Get list of recipient client id.
                string[] clientIds = null;
                string[] groups = null;
                List<string> cloudMessageClientIds = null;

                // Get recipients list.
                if (mqMessage.RecipientIds != null && mqMessage.RecipientIds.Count > 0)
                {
                    var recipientIds = mqMessage.RecipientIds;
                    clientIds = await unitOfWork.SignalrConnections
                        .Search(x => recipientIds.Contains(x.UserId))
                        .Select(x => x.ClientId)
                        .ToArrayAsync();

                    cloudMessageClientIds = await unitOfWork.UserDeviceTokens
                        .Search(x => recipientIds.Contains(x.UserId))
                        .Select(x => x.DeviceId)
                        .ToListAsync();
                }

                // Get targeted groups.
                if (mqMessage.TargetedGroups != null && mqMessage.TargetedGroups.Count > 0)
                {
                    groups = mqMessage.TargetedGroups
                        .ToArray();
                }
                
                // Cloud message async task.
                var broadcastSignalrMessageTask = SendSignalrMessageAsync(clientIds, groups,
                    realTimeService,
                    mqMessage,
                    CancellationToken.None);

                // Send cloud messages.
                var broadcastCloudMessageTask = SendCloudMessagingAsync(cloudMessageClientIds, cloudMessagingService,
                    mqMessage, CancellationToken.None);

                // Add notification message.
                var addNotificationMessageTask =
                    AddNotificationMessageAsync(timeService, unitOfWork, mqMessage, CancellationToken.None);

                await Task.WhenAll(broadcastSignalrMessageTask, broadcastCloudMessageTask, addNotificationMessageTask);
            });
        }

        /// <summary>
        /// Send cloud message asynchronously.
        /// </summary>
        /// <returns></returns>
        protected virtual async Task<HttpResponseMessage> SendCloudMessagingAsync(List<string> cloudMessageClientIds,
            ICloudMessagingService cloudMessagingService,
            InternalMqMessage internalMqMessage,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Message queue setting is empty.
            if (internalMqMessage == null)
                return null;

            // Recipients list is empty.
            if (internalMqMessage.RecipientIds == null || internalMqMessage.RecipientIds.Count < 1)
                return null;
            
            // Initialize web fcm message.
            var webFcmMessageContent = new WebFcmMessageContent();
            //webFcmMessageContent.Icon = "https://i.ibb.co/gwW79Km/Racoon-Mario.png";
            //webFcmMessageContent.Title = "TITLE_TITLE";
            //webFcmMessageContent.Body = "TITLE_BODY";
            
            var cloudMessage = new FcmMessage<object>();
            cloudMessage.RegistrationIds = cloudMessageClientIds;
            cloudMessage.CollapseKey = Guid.NewGuid().ToString("D");
            cloudMessage.Data = internalMqMessage.ExtraInfo;
            cloudMessage.Notification = webFcmMessageContent;
            return await cloudMessagingService.SendAsync(cloudMessage, cancellationToken);
        }

        /// <summary>
        /// Send signalr message.
        /// </summary>
        /// <returns></returns>
        protected virtual async Task SendSignalrMessageAsync(string[] recipientIds, string[] groups,
            IRealTimeService realTimeService,
            InternalMqMessage internalMqMessage,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (internalMqMessage == null)
                return;

            // List of task
            var backgroundTasks = new List<Task>();

            // Recipient ids are defined.
            if (recipientIds != null && recipientIds.Length > 0)
            {
                // Send messages to clients to task.
                var broadcastMqToClientsTask = realTimeService
                    .SendRealTimeMessageToClientsAsync(recipientIds, internalMqMessage.EventName, internalMqMessage,
                        CancellationToken.None);

                backgroundTasks.Add(broadcastMqToClientsTask);
            }
            
            if (groups != null && groups.Length > 0)
            {
                var broadcastMqToGroupsTask = _notificationHubContext
                    .Clients
                    .Groups(groups)
                    .SendCoreAsync(internalMqMessage.EventName, new[] { internalMqMessage }, cancellationToken);

                backgroundTasks.Add(broadcastMqToGroupsTask);
            }

            if (backgroundTasks.Count > 0)
                await Task.WhenAll(backgroundTasks.ToArray());
        }

        /// <summary>
        /// Add notification message asynchronously.
        /// </summary>
        /// <param name="baseTimeService"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="internalMqMessage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected virtual async Task AddNotificationMessageAsync(
            IBaseTimeService baseTimeService,
            IRealTimeUnitOfWork unitOfWork,
            InternalMqMessage internalMqMessage,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (internalMqMessage == null || internalMqMessage.RecipientIds == null)
                return;

            var recipientIds = internalMqMessage.RecipientIds;
            if (recipientIds.Count < 1)
                return;

            // Add notification message.
            foreach (var recipientId in recipientIds)
            {
                var notificationMessage = new NotificationMessage(recipientId, NotificationStatus.Unseen,
                    baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow), JsonConvert.SerializeObject(internalMqMessage.ExtraInfo),
                    internalMqMessage.Message);
                unitOfWork.NotificationMessages.Insert(notificationMessage);
            }

            await unitOfWork.CommitAsync(cancellationToken);
        }

        #endregion
    }
}