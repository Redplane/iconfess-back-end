﻿using AuthenticationDb.Enumerations;
using Newtonsoft.Json;

namespace AuthenticationDb.Models.Entities
{
    public class OperationToken
    {
        #region Properties

        public int UserId { get; set; }

        public string Code { get; set; }

        public double IssuedTime { get; set; }

        public double? ExpiredTime { get; set; }

        public OperationTokenKinds Kinds { get; set; }

        #endregion

        #region Navigation properties

        /// <summary>
        /// Owner of operation token.
        /// </summary>
        [JsonIgnore]
        public virtual User User { get; set; }

        #endregion
    }
}