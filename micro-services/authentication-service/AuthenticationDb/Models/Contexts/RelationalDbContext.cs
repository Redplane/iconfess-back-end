﻿using System.Linq;
using AuthenticationDb.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace AuthenticationDb.Models.Contexts
{
    public class RelationalDbContext : DbContext
    {
        #region Constructors

        /// <summary>
        ///     Initiate database context with connection string.
        /// </summary>
        public RelationalDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        ///     List of accounts in database.
        /// </summary>
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<OperationToken> OperationTokens { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Callback which is fired when model is being created.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // User table.
            AddUserTable(modelBuilder);
            
            // Activation token table.
            AddOperationTokenTable(modelBuilder);

            // Use model builder to specify composite primary keys.
            // Composite primary keys configuration

            // This is for remove pluralization naming convention in database defined by Entity Framework.
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
                entity.Relational().TableName = entity.DisplayName();

            // Disable cascade delete.
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
        }

        #endregion

        #region Tables initialization

        /// <summary>
        ///     Initialize user table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddUserTable(ModelBuilder modelBuilder)
        {
            // Get entity.
            var user = modelBuilder.Entity<User>();

            // Set primary key.
            user.HasKey(x => x.Id);
            user.Property(x => x.Id).UseSqlServerIdentityColumn();

            user.Property(x => x.Email).IsRequired();
            user.Property(x => x.Nickname).IsRequired();
        }
        
        /// <summary>
        ///     Add activation token table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddOperationTokenTable(ModelBuilder modelBuilder)
        {
            var operationToken = modelBuilder.Entity<OperationToken>();

            // Mark code as key.
            operationToken.HasKey(x => x.UserId);
            operationToken.Property(x => x.Code)
                .IsRequired();

            operationToken.HasOne(x => x.User)
                .WithOne(x => x.OpeationToken)
                .HasForeignKey<OperationToken>(x => x.UserId);
        }

        #endregion
    }
}