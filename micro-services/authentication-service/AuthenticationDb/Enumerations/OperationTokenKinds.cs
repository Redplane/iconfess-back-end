﻿namespace AuthenticationDb.Enumerations
{
    public enum OperationTokenKinds
    {
        RequestPasswordReset,
        ActivateUser
    }
}