﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthenticationMicroService.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "User",
                table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: false),
                    Nickname = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Kind = table.Column<int>(nullable: false),
                    Role = table.Column<int>(nullable: false),
                    Signature = table.Column<string>(nullable: true),
                    JoinedTime = table.Column<double>(nullable: false),
                    LastModifiedTime = table.Column<double>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_User", x => x.Id); });

            migrationBuilder.CreateTable(
                "AccessToken",
                table => new
                {
                    Code = table.Column<string>(nullable: false),
                    OwnerId = table.Column<int>(nullable: false),
                    IssuedTime = table.Column<double>(nullable: false),
                    ExpiredTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessToken", x => x.Code);
                    table.ForeignKey(
                        "FK_AccessToken_User_OwnerId",
                        x => x.OwnerId,
                        "User",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "ActivationToken",
                table => new
                {
                    Code = table.Column<string>(nullable: false),
                    OwnerId = table.Column<int>(nullable: false),
                    IssuedTime = table.Column<double>(nullable: false),
                    ExpiredTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivationToken", x => x.Code);
                    table.ForeignKey(
                        "FK_ActivationToken_User_OwnerId",
                        x => x.OwnerId,
                        "User",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "ForgotPasswordToken",
                table => new
                {
                    Code = table.Column<string>(nullable: false),
                    OwnerId = table.Column<int>(nullable: false),
                    IssuedTime = table.Column<double>(nullable: false),
                    ExpiredTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForgotPasswordToken", x => x.Code);
                    table.ForeignKey(
                        "FK_ForgotPasswordToken_User_OwnerId",
                        x => x.OwnerId,
                        "User",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_AccessToken_OwnerId",
                "AccessToken",
                "OwnerId");

            migrationBuilder.CreateIndex(
                "IX_ActivationToken_OwnerId",
                "ActivationToken",
                "OwnerId");

            migrationBuilder.CreateIndex(
                "IX_ForgotPasswordToken_OwnerId",
                "ForgotPasswordToken",
                "OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "AccessToken");

            migrationBuilder.DropTable(
                "ActivationToken");

            migrationBuilder.DropTable(
                "ForgotPasswordToken");

            migrationBuilder.DropTable(
                "User");
        }
    }
}