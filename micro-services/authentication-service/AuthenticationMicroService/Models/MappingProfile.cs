﻿using AuthenticationDb.Models.Entities;
using AuthenticationShared.ViewModels.User;
using AutoMapper;
using ServiceShared.ViewModels;

namespace AuthenticationMicroService.Models
{
    public class MappingProfile : Profile
    {
        #region Constructor

        /// <summary>
        ///     Initialize automapper mapping profile.
        /// </summary>
        public MappingProfile()
        {
            // Post mapping.
            //CreateMap<AddPostViewModel, Post>();
            CreateMap<User, UserViewModel>();
            CreateMap<AddBasicUserViewModel, User>();
        }

        #endregion
    }
}