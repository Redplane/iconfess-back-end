﻿using System.Collections.Generic;
using AuthenticationBusiness.Domains;
using AuthenticationBusiness.Interfaces;
using AuthenticationBusiness.Interfaces.Domains;
using AuthenticationDb.Interfaces;
using AuthenticationDb.Models.Contexts;
using AuthenticationDb.Models.Entities;
using AuthenticationDb.Services;
using AuthenticationMicroService.Configs;
using AuthenticationMicroService.Interfaces.Services;
using AuthenticationMicroService.Models;
using AuthenticationMicroService.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.SwaggerGeneration.Processors.Security;
using ServiceShared.Constants;
using ServiceShared.Extensions;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.Services;

namespace AuthenticationMicroService
{
    public class Startup
    {
        #region Properties

        /// <summary>
        ///     Instance stores configuration of application.
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        ///     Hosting environement configuration.
        /// </summary>
        public IHostingEnvironment HostingEnvironment { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when application starts.
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddJsonFile($"dbSetting.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            // Set hosting environment.
            HostingEnvironment = env;
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add services DI to app.
            AddServices(services);

            // Bootstrap MVC app.
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            // Use in-app exception handler.
            app.UseCustomizedExceptionHandler(env);

            app.UseSwagger();
            app.UseSwaggerUi3();
            app.UseMvc();
        }

        /// <summary>
        ///     Add dependency injection of services to app.
        /// </summary>
        private void AddServices(IServiceCollection services)
        {
            // Add entity framework to services collection.
            var sqlConnection = "";

#if USE_SQLITE
            sqlConnection = Configuration.GetConnectionString("sqliteConnectionString");
            services.AddDbContext<RelationalDatabaseContext>(
                options => options.UseSqlite(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_AZURE_SQL
            sqlConnection = Configuration.GetConnectionString("azureSqlServerConnectionString");
            services.AddDbContext<RelationalDatabaseContext>(
                options => options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_IN_MEMORY
            services.AddOptions<DbSeedOption>();
            services.AddDbContext<InMemoryRelationalDbContext>(
                options => options.UseInMemoryDatabase("iConfess")
                    .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning)));

            services.AddMockingRecords(HostingEnvironment);
            var bDbCreated = false;
            services.AddScoped<DbContext>(context =>
            {
                var inMemoryContext = context.GetService<InMemoryRelationalDbContext>();
                if (!bDbCreated)
                {
                    inMemoryContext.Database.EnsureCreated();
                    bDbCreated = true;
                }
                return context.GetService<InMemoryRelationalDbContext>();
            });
#else
            sqlConnection = Configuration.GetConnectionString("sqlServerConnectionString");
            services.AddDbContext<RelationalDbContext>(options =>
                options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(AuthenticationMicroService))));
            services.AddScoped<DbContext, RelationalDbContext>();
#endif

            // Get app configuration.
            services.Configure<AppJwtModel>(Configuration.GetSection(AppConfigKeyConstants.AppJwt));
            //services.Configure<ApplicationSetting>(Configuration.GetSection(nameof(ApplicationSetting)));

            // Injections configuration.
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IAuthenticationUnitOfWork, AuthenticationUnitOfWork>();
            services.AddScoped<IBaseRelationalDbService, BaseRelationalDbService>();
            services.AddScoped<ICaptchaService, CaptchaService>();
            services.AddScoped<IBaseEncryptionService, EncryptionService>();
            services.AddScoped<IExternalAuthenticationService, ExternalAuthenticationService>();
            services.AddScoped<IBaseTimeService, BaseTimeService>();
            services.AddScoped<IUserDomain, UserDomain>();
            services.AddScoped<IOperationTokenDomain, OperationTokenDomain>();
            services.AddScoped<IAuthenticationProfileService, AuthenticationProfileService>();

            // Register HttpContextAccessor.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Store user information in cache
            services.AddSingleton<IBaseKeyValueCacheService<int, User>, ProfileCacheService>();

            AuthenticationCacheClientConfigs.Register(Configuration, services);

            // Add automaper configuration.
            services.AddAutoMapper(options => options.AddProfile(typeof(MappingProfile)));

            // Lowered cased route configuration
            services.Configure<RouteOptions>(options => { options.LowercaseUrls = true; });

            // Add swagger document.
            services.AddSwaggerDocument(settings =>
            {
                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                settings.SerializerSettings = jsonSerializerSettings;

                settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("API Key"));
                settings.DocumentProcessors.Add(new SecurityDefinitionAppender("API Key",
                    new SwaggerSecurityScheme
                    {
                        Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        Description = "Copy 'Bearer ' + valid JWT token into field",
                        In = SwaggerSecurityApiKeyLocation.Header
                    }));
            });

            services.AddHttpClient();
        }

        #endregion
    }
}