﻿using System.Collections.Generic;
using System.Data;
using AuthenticationMicroService.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceShared.Constants;
using ServiceShared.Interfaces;
using ServiceShared.Models.CachedEntities;
using ServiceShared.Services;
using ServiceStack.OrmLite;

namespace AuthenticationMicroService.Configs
{
    public class AuthenticationCacheClientConfigs
    {
        /// <summary>
        ///     Register redis configuration for caching.
        /// </summary>
        public static void Register(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            // Get redis configuration.
            var cacheClients = new Dictionary<string, string>();
            configuration
                .GetSection(AuthenticationConfigKeyConstant.CacheClients)
                .Bind(cacheClients);

            var accessTokenCacheInstance = new OrmLiteConnectionFactory(cacheClients[AuthenticationCacheClientKeyConstant.AccessTokenSqlClient], SqlServer2017Dialect.Provider);
            var accessTokenDbConnection = new AccessTokenDbConnection(accessTokenCacheInstance);
            accessTokenDbConnection.Open();
            accessTokenDbConnection.CreateTableIfNotExists<AccessToken>();

            // Add redis client manager cache service.
            serviceCollection.AddSingleton<IAccessTokenDbConnection, AccessTokenDbConnection>(x => accessTokenDbConnection);

            // Add access token - user redis cache.
            
        }
    }
}