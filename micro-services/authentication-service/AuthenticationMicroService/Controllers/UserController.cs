﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AuthenticationBusiness.Interfaces.Domains;
using AuthenticationDb.Enumerations;
using AuthenticationDb.Models.Entities;
using AuthenticationMicroService.Interfaces.Services;
using AuthenticationMicroService.ViewModels;
using AuthenticationModel.Enumerations;
using AuthenticationModel.Models;
using AuthenticationShared.Resources;
using AuthenticationShared.ViewModels.User;
using AutoMapper;
using ClientShared.Enumerations;
using ClientShared.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ServiceShared.Authentications.ActionFilters;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using SkiaSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AuthenticationMicroService.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        #region Constructors

        /// <summary>
        ///     Initiate controller with injectors.
        /// </summary>
        public UserController(IOptions<AppJwtModel> appJwtModelOptions,
            ICaptchaService captchaService, IBaseEncryptionService encryptionService,
            IAuthenticationProfileService baseProfileService,
            IBaseTimeService baseTimeService,
            IMapper mapper,
            IUserDomain userDomain, IOperationTokenDomain operationTokenDomain)
        {
            _appJwtModel = appJwtModelOptions.Value;

            _captchaService = captchaService;
            _mapper = mapper;
            _userDomain = userDomain;
            _operationTokenDomain = operationTokenDomain;
            _timeService = baseTimeService;
            _encryptionService = encryptionService;
            _authenticationProfileService = baseProfileService;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Service which is for checking captcha.
        /// </summary>
        private readonly ICaptchaService _captchaService;

        /// <summary>
        ///     Service which is for handling user.
        /// </summary>
        private readonly IUserDomain _userDomain;

        private readonly IOperationTokenDomain _operationTokenDomain;

        private readonly IBaseTimeService _timeService;

        private readonly IMapper _mapper;

        private readonly IBaseEncryptionService _encryptionService;

        private readonly AppJwtModel _appJwtModel;

        private readonly IAuthenticationProfileService _authenticationProfileService;

        #endregion

        #region Methods

        /// <summary>
        ///     Use specific condition to check whether account is available or not.
        ///     If account is valid for logging into system, access token will be provided.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("basic-login")]
        [AllowAnonymous]
        public virtual async Task<IActionResult> BasicLogin([FromBody] LoginViewModel model)
        {
            #region Parameters validation

            // Parameter hasn't been initialized.
            if (model == null)
            {
                model = new LoginViewModel();
                TryValidateModel(model);
            }

            // Invalid modelstate.
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Captcha validation

            // Verify the captcha.
            var bIsCaptchaValid =
                await _captchaService.IsCaptchaValidAsync(model.CaptchaCode, null, CancellationToken.None);
            if (!bIsCaptchaValid)
                return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.CaptchaInvalid));

            #endregion

            #region Find user information from cache

            // TODO: Check MD5 hash.

            // Initialize jwt.
            JwtViewModel jsonWebToken;

            // Find access token by user information.
            var hashedPassword = _encryptionService.Md5Hash(model.Password);

            // If information is available. Return it to client directly.
            var accessToken =
                await _userDomain.FindUserAccessTokenAsync(model.Email, hashedPassword, UserKind.Basic,
                    CancellationToken.None);
            if (accessToken != null)
                return Ok(_userDomain.ConvertAccessTokenToJwt(accessToken));

            #endregion

            #region Login & generate json web token

            // Get user instance.
            var user = await _userDomain.LoginAsync(model);

            // Initialize jwt token.
            jsonWebToken = _userDomain.GenerateJwt(user);
            await _userDomain.AddUserAccessTokenAsync(user);

            #endregion

            return Ok(jsonWebToken);
        }

        [HttpPost("basic-register")]
        [AllowAnonymous]
        public virtual async Task<IActionResult> RegisterBasicUser([FromBody] AddBasicUserViewModel user)
        {
            #region viewModel Validation

            if (user == null)
            {
                user = new AddBasicUserViewModel();
                TryValidateModel(user);
            }

            // Invalid modelstate.
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Captcha validation

            // Verify the captcha.
            var bIsCaptchaValid =
                await _captchaService.IsCaptchaValidAsync(user.CapchaCode, null, CancellationToken.None);
            if (!bIsCaptchaValid)
                return StatusCode((int)HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.CaptchaInvalid));

            #endregion

            #region Create New Basic User            

            // Set Md5 encription for password
            user.PassWord = _encryptionService.Md5Hash(user.PassWord);

            // Register New User
            await _userDomain.AddBasicUser(user);

            #endregion

            return Ok();
        }

        /// <summary>
        ///     Use specific conditions to login into Google authentication system.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost("google-login")]
        [AllowAnonymous]
        public virtual async Task<IActionResult> GoogleLogin([FromBody] GoogleLoginViewModel info)
        {
            #region Request params validation

            if (info == null)
            {
                info = new GoogleLoginViewModel();
                TryValidateModel(info);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Google login

            // Sign user into system using his/her google account.
            var user = await _userDomain.GoogleLoginAsync(info);

            // Generate user json web token.
            var jsonWebToken = _userDomain.GenerateJwt(user);
            await _userDomain.AddUserAccessTokenAsync(user);

            #endregion

            return Ok(jsonWebToken);
        }

        /// <summary>
        ///     Use specific information to sign into system using facebook account.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost("facebook-login")]
        [AllowAnonymous]
        public virtual async Task<IActionResult> FacebookLogin([FromBody] FacebookLoginViewModel info)
        {
            #region Request parameters validation

            // Information hasn't been initialized.
            if (info == null)
            {
                info = new FacebookLoginViewModel();
                TryValidateModel(info);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Facebook login & token generate

            // Do facebook login.
            var user = await _userDomain.FacebookLoginAsync(info);

            // Initialize access token.
            var jsonWebToken = _userDomain.GenerateJwt(user);
            await _userDomain.AddUserAccessTokenAsync(user);

            #endregion

            return Ok(jsonWebToken);
        }

        /// <summary>
        ///     Request password reset.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("request-password-reset")]
        public virtual async Task<IActionResult> RequestPasswordReset([FromBody] RequestPasswordResetViewModel model)
        {
            if (model == null)
            {
                model = new RequestPasswordResetViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var loadUserCondition = new SearchUserViewModel();
            loadUserCondition.Emails = new HashSet<string> {model.Email};
            loadUserCondition.Statuses = new HashSet<UserStatus> {UserStatus.Available};

            // Find the activated user in the system.
            var user = await _userDomain.SearchUserAsync(loadUserCondition);
            if (user == null)
                return NotFound(new ApiResponse(HttpMessages.UserNotFound));

            // Get current utc time.
            var utcTime = DateTime.UtcNow;
            var expiredUtcTime = utcTime.AddHours(24);

            var operationToken = new OperationToken();
            operationToken.UserId = user.Id;
            operationToken.IssuedTime = _timeService.DateTimeUtcToUnix(utcTime);
            operationToken.ExpiredTime = _timeService.DateTimeUtcToUnix(expiredUtcTime);
            operationToken.Kinds = OperationTokenKinds.RequestPasswordReset;
            operationToken.Code = Guid.NewGuid().ToString("N");

            await _operationTokenDomain.AddOperationTokenAsync(operationToken);
            return Ok();
        }

        /// <summary>
        ///     Submit password reset.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("submit-password-reset")]
        public virtual async Task<IActionResult> SubmitPasswordReset([FromBody] SubmitPasswordResetViewModel model)
        {
            if (model == null)
            {
                model = new SubmitPasswordResetViewModel();
                TryValidateModel(model);
            }

            if (ModelState.IsValid)
                return BadRequest(ModelState);

            // Verify the captcha.
            var bIsCaptchaValid =
                await _captchaService.IsCaptchaValidAsync(model.CaptchaCode, null, CancellationToken.None);
            if (!bIsCaptchaValid)
                return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.CaptchaInvalid));

            // Find operation token.
            var loadOperationTokenCondition = new SearchOperationTokenModel();
            loadOperationTokenCondition.Codes = new HashSet<string> {model.Code};
            loadOperationTokenCondition.Kinds =
                new HashSet<UserOperationTokenKinds> {UserOperationTokenKinds.RequestPasswordReset};

            // Find operation token.
            var operationToken = await _operationTokenDomain.SearchUserOperationTokenAsync(loadOperationTokenCondition);
            if (operationToken == null)
                return NotFound(new ApiResponse(HttpMessages.CouldnotFindPasswordResetToken));

            // Get current time.
            var unixCurrentTime = _timeService.DateTimeUtcToUnix(DateTime.UtcNow);
            if (operationToken.ExpiredTime != null && operationToken.ExpiredTime > unixCurrentTime)
                return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.SessionExpired));

            // Remove reset password.
            await _operationTokenDomain.DeleteOperationTokenUserIdsAsync(new HashSet<int> {operationToken.UserId});

            // Find user and update his/her password.
            // Hash the password.
            var hashedPassword = _encryptionService.Md5Hash(model.Password);
            var user = await _userDomain.UpdateUserPasswordAsync(operationToken.UserId, hashedPassword);

            // Generate new user jwt.
            var jsonWebToken = _userDomain.GenerateJwt(user);

            // Insert a new jwt to cache.
            await _userDomain.AddUserAccessTokenAsync(user);

            return Ok(jsonWebToken);
        }

        /// <summary>
        ///     Add / edit user signature.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("signature")]
        public virtual async Task<IActionResult> AddUserSignature([FromBody] AddUserSignatureViewModel model)
        {
            if (model == null)
            {
                model = new AddUserSignatureViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Verify the captcha.
            var bIsCaptchaValid =
                await _captchaService.IsCaptchaValidAsync(model.CaptchaCode, null, CancellationToken.None);
            if (!bIsCaptchaValid)
                return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.CaptchaInvalid));

            var profile = _authenticationProfileService.GetProfile();
            var userId = model.UserId != null ? model.UserId.Value : profile.Id;

            if (profile.Role != UserRole.Admin)
                userId = profile.Id;

            await _userDomain.AddUserSignatureAsync(userId, model.Signature);
            return Ok();
        }

        /// <summary>
        ///     Upload profile asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("upload-avatar")]
        public virtual async Task<IActionResult> UploadProfilePhotoAsync([FromBody] UploadPhotoViewModel model)
        {
            #region Request parameters validation

            if (model == null)
            {
                model = new UploadPhotoViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Change user profile photo

            // Get requester profile.
            var profile = _authenticationProfileService.GetProfile();
            if (model.UserId == null)
                model.UserId = profile.Id;

            if (model.UserId != profile.Id && profile.Role != UserRole.Admin)
                return StatusCode((int) HttpStatusCode.Forbidden,
                    new ApiResponse(HttpMessages.HasNoPermissionChangeUserProfilePhoto));

            // Reflect image variable.
            var image = model.Photo;

            using (var skManagedStream = new SKManagedStream(image.OpenReadStream()))
            {
                var skBitmap = SKBitmap.Decode(skManagedStream);

                // Uplooad profile photo.

                try
                {
                    var user = await _userDomain.UploadUserPhotoAsync(model.UserId.Value, skBitmap);
                    return Ok(user);
                }
                catch (Exception exception)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new ApiResponse(HttpMessages.ImageIsInvalid));
                }
            }

            #endregion
        }

        /// <summary>
        ///     Load accounts by using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [ByPassAuthorization]
        public virtual async Task<IActionResult> SearchUsersAsync([FromBody] SearchUserViewModel condition)
        {
            if (condition == null)
            {
                condition = new SearchUserViewModel();
                TryValidateModel(condition);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Get request profile.
            var profile = _authenticationProfileService.GetProfile();

            // Ordinary users can only see available users.
            if (profile == null || profile.Role != UserRole.Admin)
                condition.Statuses = new HashSet<UserStatus> {UserStatus.Available};

            var loadUsersResult = await _userDomain.SearchUsersAsync(condition);
            return Ok(loadUsersResult);
        }

        /// <summary>
        ///     Re-send activation email
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("resend-activation-code")]
        public virtual async Task<IActionResult> RequestActivationEmailAsync(
            [FromBody] RequestUserActivationEmailViewModel model)
        {
            // Model validation
            if (model == null)
            {
                model = new RequestUserActivationEmailViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Verify the captcha.
            var bIsCaptchaValid =
                await _captchaService.IsCaptchaValidAsync(model.CaptchaCode, null, CancellationToken.None);
            if (!bIsCaptchaValid)
                return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse(HttpMessages.CaptchaInvalid));

            // Generate user activation token.
            await _userDomain.RequestUserActivationTokenAsync(model);

            return Ok();
        }

        /// <summary>
        ///     Change user password asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("password")]
        public virtual async Task<IActionResult> ChangeUserPasswordAsync([FromBody] ChangePasswordViewModel model)
        {
            // Model validation
            if (model == null)
            {
                model = new ChangePasswordViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Get user profile.
            var profile = _authenticationProfileService.GetProfile();

            // Change user password asynchronously.
            var user = await _userDomain.ChangePasswordAsync(profile.Id, model);

            // Generate json web token.
            var jwtToken = _userDomain.GenerateJwt(user);
            await _userDomain.AddUserAccessTokenAsync(user);
            return Ok(jwtToken);
        }

        #endregion
    }
}