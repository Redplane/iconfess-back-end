﻿namespace AuthenticationMicroService.Constants
{
    public class AuthenticationCacheClientKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Profile redis client.
        /// </summary>
        public const string AccessTokenSqlClient = "accessTokenSqlCacheClient";


        /// <summary>
        ///     Configuration of Google OAuth.
        /// </summary>
        public const string GoogleCredential = "googleCredential";

        /// <summary>
        ///     Configuration of facebook credential.
        /// </summary>
        public const string FacebookCredential = "facebookCredential";

        #endregion
    }
}