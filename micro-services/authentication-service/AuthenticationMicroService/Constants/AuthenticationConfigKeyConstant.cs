﻿namespace AuthenticationMicroService.Constants
{
    public class AuthenticationConfigKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Configuration key of jwt.
        /// </summary>
        public const string AppJwt = "appJwt";
        
        /// <summary>
        ///     Redis clients.
        /// </summary>
        public const string CacheClients = "authenticationCacheClients";

        /// <summary>
        /// Pusher real-time channels.
        /// </summary>
        public const string PusherRealTimeChannels = "pusherRealTimeChannels";

        #endregion
    }
}