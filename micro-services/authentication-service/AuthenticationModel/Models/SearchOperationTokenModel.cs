﻿using System.Collections.Generic;
using AuthenticationModel.Enumerations;
using ClientShared.Models;

namespace AuthenticationModel.Models
{
    public class SearchOperationTokenModel
    {
        #region Properties

        /// <summary>
        /// User indexes.
        /// </summary>
        public HashSet<int> UserIds { get; set; }

        public HashSet<string> Codes { get; set; }

        public Range<double?, double?> IssuedTime { get; set; }

        public Range<double?, double?> ExpiredTime { get; set; }

        public HashSet<UserOperationTokenKinds> Kinds { get; set; }

        public Pagination Pagination { get; set; }

        public bool IsTotalRecordIgnored { get; set; } = true;

        #endregion
    }
}