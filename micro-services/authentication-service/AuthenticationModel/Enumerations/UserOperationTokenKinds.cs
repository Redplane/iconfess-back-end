﻿namespace AuthenticationModel.Enumerations
{
    public enum UserOperationTokenKinds
    {
        RequestPasswordReset,
        ActivateUser
    }
}