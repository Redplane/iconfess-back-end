﻿using System.ComponentModel.DataAnnotations;

namespace AuthenticationShared.ViewModels.User
{
    public class AddUserSignatureViewModel
    {
        #region Properties

        public int? UserId { get; set; }

        /// <summary>
        ///     User signature.
        /// </summary>
        [Required]
        public string Signature { get; set; }

        /// <summary>
        /// Captcha code.
        /// </summary>
        [Required]
        public string CaptchaCode { get; set; }

        #endregion
    }
}