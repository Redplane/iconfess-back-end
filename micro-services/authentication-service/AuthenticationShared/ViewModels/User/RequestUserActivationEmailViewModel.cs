﻿using System.ComponentModel.DataAnnotations;

namespace AuthenticationShared.ViewModels.User
{
    public class RequestUserActivationEmailViewModel
    {
        #region Properties

        /// <summary>
        ///     email of account
        /// </summary>
        [Required]
        public string Email { get; set; }

        [Required]
        public string CaptchaCode { get; set; }

        #endregion
    }
}