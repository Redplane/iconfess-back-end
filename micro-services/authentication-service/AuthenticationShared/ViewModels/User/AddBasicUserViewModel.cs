﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AuthenticationShared.ViewModels.User
{
    public class AddBasicUserViewModel
    {
        #region Properties

        /// <summary>
        /// Email of user
        /// </summary>
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Password of user
        /// </summary>
        [Required]
        public string PassWord { get; set; }

        /// <summary>
        /// Username of user
        /// </summary>
        [Required]
        public string NickName { get; set; }

        /// <summary>
        /// Capcha code input from user
        /// </summary>
        [Required]
        public string CapchaCode { get; set; }

        #endregion
    }
}
