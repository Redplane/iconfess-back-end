﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AuthenticationBusiness.Interfaces;
using AuthenticationBusiness.Interfaces.Domains;
using AuthenticationDb.Enumerations;
using AuthenticationDb.Interfaces;
using AuthenticationDb.Models.Entities;
using AuthenticationShared.Resources;
using AuthenticationShared.ViewModels.User;
using AutoMapper;
using ClientShared.Enumerations;
using ClientShared.Enumerations.Order;
using ClientShared.Models;
using ClientShared.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ServiceShared.Exceptions;
using ServiceShared.Interfaces;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.Models.CachedEntities;
using ServiceShared.ViewModels;
using ServiceStack.OrmLite;
using SkiaSharp;

namespace AuthenticationBusiness.Domains
{
    public class UserDomain : IUserDomain
    {
        #region Constructors

        public UserDomain(IBaseEncryptionService encryptionService,
            IAuthenticationUnitOfWork unitOfWork,
            IExternalAuthenticationService externalAuthenticationService,
            IBaseTimeService baseTimeService,
            IBaseRelationalDbService relationalDbService,
            IAccessTokenDbConnection accessTokenDbConnection,
            IMapper mapper,
            IOptions<AppJwtModel> appJwt
        )
        {
            _encryptionService = encryptionService;
            _unitOfWork = unitOfWork;
            _externalAuthenticationService = externalAuthenticationService;
            _baseTimeService = baseTimeService;
            _relationalDbService = relationalDbService;
            _appJwt = appJwt.Value;
            _mapper = mapper;
            _accessTokenDbConnection = accessTokenDbConnection;
        }

        #endregion

        #region Properties

        private readonly IBaseEncryptionService _encryptionService;

        private readonly IExternalAuthenticationService _externalAuthenticationService;

        private readonly IAuthenticationUnitOfWork _unitOfWork;

        private readonly IBaseTimeService _baseTimeService;

        //private readonly ApplicationSetting _applicationSettings;

        private readonly IBaseRelationalDbService _relationalDbService;

        private readonly AppJwtModel _appJwt;

        private readonly IAccessTokenDbConnection _accessTokenDbConnection;

        private readonly IMapper _mapper;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> LoginAsync(LoginViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Hash the password first.
            var hashedPassword = _encryptionService.Md5Hash(model.Password);

            // Search for account which is active and information is correct.
            var users = _unitOfWork.Users.Search();
            users = users.Where(x =>
                x.Email.Equals(model.Email, StringComparison.InvariantCultureIgnoreCase) &&
                x.Password.Equals(hashedPassword, StringComparison.InvariantCultureIgnoreCase) &&
                x.Kind == UserKind.Basic);

            // Find the first account in database.
            var user = await users.FirstOrDefaultAsync(cancellationToken);
            if (user == null)
                throw new ApiException(HttpMessages.UserNotFound, HttpStatusCode.NotFound);

            switch (user.Status)
            {
                case UserStatus.Pending:
                    throw new ApiException(HttpMessages.AccountIsPending, HttpStatusCode.Forbidden);
                case UserStatus.Disabled:
                    throw new ApiException(HttpMessages.AccountIsDisabled, HttpStatusCode.Forbidden);
            }

            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> GoogleLoginAsync(GoogleLoginViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get the profile information.
            var profile = await _externalAuthenticationService.GetGoogleBasicProfileAsync(model.IdToken);
            if (profile == null)
                throw new ApiException(HttpMessages.GoogleCodeIsInvalid, HttpStatusCode.Forbidden);

            // Find accounts by searching for email address.
            var users = _unitOfWork.Users.Search();
            users = users.Where(x => x.Email.Equals(profile.Email));

            // Get the first matched account.
            var user = await users.FirstOrDefaultAsync(cancellationToken);

            // Account is available in the system. Check its status.
            if (user != null)
            {
                // Prevent account from logging into system because it is pending.
                if (user.Status == UserStatus.Pending)
                    throw new ApiException(HttpMessages.AccountIsPending, HttpStatusCode.Forbidden);

                // Prevent account from logging into system because it is deleted.
                if (user.Status == UserStatus.Disabled)
                    throw new ApiException(HttpMessages.AccountIsDisabled, HttpStatusCode.Forbidden);
            }
            else
            {
                // Initialize account instance.
                user = new User();

#if USE_IN_MEMORY
                user.Id = _unitOfWork.Accounts.Search().OrderByDescending(x => x.Id).Select(x => x.Id)
                              .FirstOrDefault() + 1;
#endif
                user.Email = profile.Email;
                user.Nickname = profile.Name;
                user.Role = UserRole.User;
                user.Photo = profile.Picture;
                user.JoinedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
                user.Kind = UserKind.Google;
                user.Status = UserStatus.Available;

                // Add account to database.
                _unitOfWork.Users.Insert(user);
                await _unitOfWork.CommitAsync(cancellationToken);
            }
            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> FacebookLoginAsync(FacebookLoginViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find token information.
            var tokenInfo = await _externalAuthenticationService.GetFacebookTokenInfoAsync(model.AccessToken);
            if (tokenInfo == null || string.IsNullOrWhiteSpace(tokenInfo.AccessToken))
                throw new ApiException(HttpMessages.FacebookCodeIsInvalid, HttpStatusCode.Forbidden);

            // Get the profile information.
            var profile = await _externalAuthenticationService.GetFacebookBasicProfileAsync(tokenInfo.AccessToken);
            if (profile == null)
                throw new ApiException(HttpMessages.GoogleCodeIsInvalid, HttpStatusCode.Forbidden);


            // Find accounts by searching for email address.
            var accounts = _unitOfWork.Users.Search();
            accounts = accounts.Where(x => x.Email.Equals(profile.Email));

            // Get the first matched account.
            var account = await accounts.FirstOrDefaultAsync(cancellationToken);

            // Account is available in the system. Check its status.
            if (account != null)
            {
                // Prevent account from logging into system because it is pending.
                if (account.Status == UserStatus.Pending)
                    throw new ApiException(HttpMessages.AccountIsPending, HttpStatusCode.Forbidden);

                // Prevent account from logging into system because it is deleted.
                if (account.Status == UserStatus.Disabled)
                    throw new ApiException(HttpMessages.AccountIsPending, HttpStatusCode.Forbidden);
            }
            else
            {
                // Initialize account instance.
                account = new User();
                account.Email = profile.Email;
                account.Nickname = profile.FullName;
                account.Role = UserRole.User;
                account.JoinedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
                account.Kind = UserKind.Facebook;

                // Add account to database.
                _unitOfWork.Users.Insert(account);
                await _unitOfWork.CommitAsync(cancellationToken);
            }

            return account;
        }

        /// <summary>
        ///     Search users using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<IList<User>>> SearchUsersAsync(SearchUserViewModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var users = GetUsers(condition);

            // Sort by properties.
            if (condition.Sort != null)
                users =
                    _relationalDbService.Sort(users, condition.Sort.Direction,
                        condition.Sort.Property);
            else
                users = _relationalDbService.Sort(users, ClientShared.Enumerations.SortDirection.Decending,
                    UserSort.JoinedTime);

            // Result initialization.
            var loadUsersResult = new SearchResult<IList<User>>();
            loadUsersResult.Total = await users.CountAsync(cancellationToken);
            loadUsersResult.Records = await _relationalDbService.Paginate(users, condition.Pagination)
                .ToListAsync(cancellationToken);
            return loadUsersResult;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> SearchUserAsync(SearchUserViewModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get list of possible user.
            var users = GetUsers(condition);
            return await users.FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> UpdateUserPasswordAsync(int userId, string hashedPassword,
            CancellationToken cancellationToken)
        {
            var loadUserConditions = new SearchUserViewModel();
            loadUserConditions.Ids = new HashSet<int> {userId};
            loadUserConditions.Statuses = new HashSet<UserStatus> {UserStatus.Available};

            var user = await GetUsers(loadUserConditions).FirstOrDefaultAsync(cancellationToken);
            if (user == null)
                throw new ApiException(HttpStatusCode.NotFound, ExceptionMessage.UserNotFound);

            user.Password = hashedPassword;
            user.LastModifiedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
            await _unitOfWork.CommitAsync(cancellationToken);
            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="photo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> UploadUserPhotoAsync(int id, SKBitmap photo,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find user.
            var user = await _unitOfWork.Users.FirstOrDefaultAsync(
                x => x.Id == id && x.Status == UserStatus.Available, cancellationToken);

            if (user == null)
                throw new ApiException(HttpMessages.UserNotFound, HttpStatusCode.NotFound);

            // Resize image to 512x512 size.
            var resizedSkBitmap = photo.Resize(new SKImageInfo(512, 512), SKFilterQuality.High);

            // Initialize file name.
            var fileName = $"{Guid.NewGuid():D}.png";

            using (var skImage = SKImage.FromBitmap(resizedSkBitmap))
            using (var skData = skImage.Encode(SKEncodedImageFormat.Png, 100))
            using (var memoryStream = new MemoryStream())
            {
                skData.SaveTo(memoryStream);

                // TODO: Upload image to hosting.

                //var vgySuccessRespone = await _vgyService.UploadAsync<VgySuccessResponse>(memoryStream.ToArray(),
                //    "image/png", fileName,
                //    CancellationToken.None);

                //// Response is empty.
                //if (vgySuccessRespone == null || vgySuccessRespone.IsError)
                //    throw new ApiException(HttpMessages.ImageIsInvalid, HttpStatusCode.Forbidden);

                //user.Photo = vgySuccessRespone.ImageUrl;
            }

            // Save changes into database.
            await _unitOfWork.CommitAsync(cancellationToken);
            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="signature"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<User> AddUserSignatureAsync(int userId, string signature,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var users = _unitOfWork.Users
                .Search(x => x.Id == userId && x.Status == UserStatus.Available);
            var user = await users.FirstOrDefaultAsync(cancellationToken);

            if (user == null)
                throw new ApiException(HttpStatusCode.NotFound, HttpMessages.UserNotFound);

            user.Signature = signature;
            await _unitOfWork.CommitAsync(cancellationToken);
            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task RequestUserActivationTokenAsync(
            RequestUserActivationEmailViewModel model, CancellationToken cancellationToken = default(CancellationToken))
        {
            #region Search for user

            var user = await _unitOfWork.Users.FirstOrDefaultAsync(
                x => x.Email.Equals(model.Email, StringComparison.InvariantCultureIgnoreCase) &&
                     x.Status == UserStatus.Pending && x.Kind == UserKind.Basic, cancellationToken);
            if (user == null)
                return;

            #endregion

            #region Token generation

            using (var dbTransaction = _unitOfWork.BeginTransactionScope())
            {
                // Get current time
                var utcTime = DateTime.UtcNow;
                var expiredTime = _baseTimeService.DateTimeUtcToUnix(utcTime.AddDays(1));

                // Remove the existing token.
                var operationTokens = _unitOfWork.OperationTokens.Search(x => x.UserId == user.Id);
                _unitOfWork.OperationTokens.Remove(operationTokens);

                var operationToken = new OperationToken();
                operationToken.Code = Guid.NewGuid().ToString("N");
                operationToken.IssuedTime = _baseTimeService.DateTimeUtcToUnix(utcTime);
                operationToken.ExpiredTime = expiredTime;
                operationToken.Kinds = OperationTokenKinds.ActivateUser;
                operationToken.UserId = user.Id;
                _unitOfWork.OperationTokens.Insert(operationToken);

                // Save changes asychronously.
                await _unitOfWork.CommitAsync(cancellationToken);
                dbTransaction.Commit();
            }

            // TODO : Send activation email.

            #endregion
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> ChangePasswordAsync(int id, ChangePasswordViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            #region Find user

            var loadUserCondition = new SearchUserViewModel();
            loadUserCondition.Ids = new HashSet<int> {id};
            loadUserCondition.Statuses = new HashSet<UserStatus> {UserStatus.Available};

            var user = await SearchUserAsync(loadUserCondition, cancellationToken);

            if (user == null)
                throw new ApiException(HttpMessages.UserNotFound, HttpStatusCode.NotFound);

            #endregion

            // Hash the curent password.
            var hashedCurrentPassword = _encryptionService.Md5Hash(model.OriginalPassword);
            if (!user.Password.Equals(hashedCurrentPassword, StringComparison.CurrentCultureIgnoreCase))
                throw new ApiException(HttpMessages.CurrentPasswordInvalid, HttpStatusCode.Forbidden);

            // Hash new password.
            user.Password = _encryptionService.Md5Hash(model.Password);

            // Save changes to database.
            await _unitOfWork.CommitAsync(cancellationToken);


            return user;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public JwtViewModel GenerateJwt(User user)
        {
            // Find current time on the system.
            var utcNow = DateTime.UtcNow;
            var utcExpiration = utcNow.AddSeconds(_appJwt.LifeTime);
            var utcExpirationTime = _baseTimeService.DateTimeUtcToUnix(utcExpiration);

            // Claims initalization.
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Aud, _appJwt.Audience));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iss, _appJwt.Issuer));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(nameof(user.Nickname), user.Nickname));
            claims.Add(new Claim(nameof(user.Id), user.Id.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Exp, $"{utcExpiration}"));

            // Write a security token.
            var jwtSecurityToken = new JwtSecurityToken(_appJwt.Issuer, _appJwt.Audience, claims,
                null, utcExpiration, _appJwt.SigningCredentials);

            // Initiate token handler which is for generating token code.
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            jwtSecurityTokenHandler.WriteToken(jwtSecurityToken);

            // Initialize jwt response.
            var jwt = new JwtViewModel();
            jwt.AccessToken = jwtSecurityTokenHandler.WriteToken(jwtSecurityToken);
            jwt.LifeTime = _appJwt.LifeTime;
            jwt.ExpiredTime = utcExpirationTime;
            jwt.IssuedTime = _baseTimeService.DateTimeUtcToUnix(utcNow);

            return jwt;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public JwtViewModel ConvertAccessTokenToJwt(AccessToken accessToken)
        {
            var jsonWebToken = new JwtViewModel();
            jsonWebToken.AccessToken = accessToken.Code;
            jsonWebToken.ExpiredTime = accessToken.ExpiredTime;
            jsonWebToken.IssuedTime = accessToken.IssuedTime;
            jsonWebToken.LifeTime = accessToken.LifeTime;

            return jsonWebToken;
        }

        /// <summary>
        ///     Get users using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        protected virtual IQueryable<User> GetUsers(SearchUserViewModel condition)
        {
            // Get all users.
            var users = _unitOfWork.Users.Search();

            // Id have been defined.
            var ids = condition.Ids;
            if (ids != null && ids.Count > 0)
            {
                ids = ids.Where(x => x > 0).ToHashSet();
                if (ids != null && ids.Count > 0)
                    users = users.Where(x => condition.Ids.Contains(x.Id));
            }

            // Email have been defined.
            var emails = condition.Emails;
            if (emails != null && emails.Count > 0)
            {
                emails = emails.Where(x => !string.IsNullOrEmpty(x)).ToHashSet();
                if (emails != null && emails.Count > 0)
                    users = users.Where(x => condition.Emails.Any(y => x.Email.Contains(y)));
            }

            // Statuses have been defined.
            var statuses = condition.Statuses;
            if (statuses != null && statuses.Count > 0)
            {
                statuses =
                    statuses.Where(x => Enum.IsDefined(typeof(UserStatus), x)).ToHashSet();
                if (statuses.Count > 0)
                    users = users.Where(x => condition.Statuses.Contains(x.Status));
            }

            // Roles have been defined.
            var roles = condition.Roles;
            if (condition.Roles != null && condition.Roles.Count > 0)
            {
                roles =
                    roles.Where(x => Enum.IsDefined(typeof(UserRole), x)).ToHashSet();
                if (roles.Count > 0)
                    users = users.Where(x => roles.Contains(x.Role));
            }

            return users;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task AddUserAccessTokenAsync(User user,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var hashedPhrase = FormatAccessTokenHashPhrase(user.Email, user.Password, user.Kind);

            // Get json web token.
            var jsonWebToken = GenerateJwt(user);

            using (var dbTransaction = _accessTokenDbConnection.OpenTransaction(IsolationLevel.ReadCommitted))
            {
                await _accessTokenDbConnection.DeleteAsync<AccessToken>(x => x.Email == user.Email, cancellationToken);

                var accessToken = new AccessToken();
                accessToken.Email = user.Email;
                accessToken.Hash = hashedPhrase;
                accessToken.Code = jsonWebToken.AccessToken;
                accessToken.LifeTime = jsonWebToken.LifeTime;
                accessToken.IssuedTime = jsonWebToken.IssuedTime;
                accessToken.ExpiredTime = jsonWebToken.ExpiredTime;

                var userViewModel = _mapper.Map<UserViewModel>(user);
                accessToken.User = JsonConvert.SerializeObject(userViewModel);

                await _accessTokenDbConnection.InsertAsync(accessToken, token: cancellationToken);
                dbTransaction.Commit();
            }
        }       

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="email"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<AccessToken> FindUserAccessTokenAsync(string email,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _accessTokenDbConnection.SingleAsync<AccessToken>(x => x.Email == email, cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="email"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="userKind"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<AccessToken> FindUserAccessTokenAsync(string email, string hashedPassword,
            UserKind userKind, CancellationToken cancellationToken = default(CancellationToken))
        {
            var hashedPhrase = FormatAccessTokenHashPhrase(email, hashedPassword, userKind);
            return await _accessTokenDbConnection.SingleAsync<AccessToken>(x => x.Hash == hashedPhrase,
                cancellationToken);
        }

        /// <summary>
        ///     Format access token hash phrase
        /// </summary>
        /// <param name="email"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="userKind"></param>
        /// <returns></returns>
        protected virtual string FormatAccessTokenHashPhrase(string email, string hashedPassword, UserKind userKind)
        {
            var phrase = $"{email}-{hashedPassword}-{userKind}";
            return _encryptionService.Md5Hash(phrase);
        }

        /// <summary>
        /// Create New Bacis User
        /// </summary>
        /// <param name="basicUser"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task AddBasicUser(AddBasicUserViewModel basicUser,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Check user has duplicated
            var userSameEmail = await _unitOfWork.Users.FirstOrDefaultAsync(user => user.Email == basicUser.Email);
            var userSameNickName = await _unitOfWork.Users.FirstOrDefaultAsync(user => user.Nickname == basicUser.NickName);

            if (userSameEmail != null || userSameNickName != null)
                throw new ApiException(HttpMessages.UserDuplicateFound, HttpStatusCode.Conflict);

            // Mapping and Initalize User
            var addUser = _mapper.Map<User>(basicUser);

            // Extend Default User Information
            addUser.Kind = UserKind.Basic;
            addUser.Role = UserRole.User;
            addUser.JoinedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
            addUser.Status = UserStatus.Available;

            // Insert new user
            _unitOfWork.Users.Insert(addUser);

            // Commit Save User           
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        #endregion
    }
}