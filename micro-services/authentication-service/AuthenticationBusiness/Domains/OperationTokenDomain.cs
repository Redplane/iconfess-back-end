﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AuthenticationBusiness.Interfaces.Domains;
using AuthenticationDb.Interfaces;
using AuthenticationDb.Models.Entities;
using AuthenticationModel.Models;
using ClientShared.Models;
using Microsoft.EntityFrameworkCore;
using ServiceShared.Interfaces.Services;

namespace AuthenticationBusiness.Domains
{
    public class OperationTokenDomain : IOperationTokenDomain
    {
        #region Properties

        private readonly IAuthenticationUnitOfWork _unitOfWork;

        private readonly IBaseRelationalDbService _baseRelationalDbService;

        #endregion

        #region Constructor

        public OperationTokenDomain(IAuthenticationUnitOfWork unitOfWork, IBaseRelationalDbService baseRelationalDbService)
        {
            _unitOfWork = unitOfWork;
            _baseRelationalDbService = baseRelationalDbService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="operationToken"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<OperationToken> AddOperationTokenAsync(OperationToken operationToken, CancellationToken cancellationToken)
        {
            using (var dbTransaction = _unitOfWork.BeginTransactionScope())
            {
                // Remove the previous operation token.
                var userId = operationToken.UserId;
                var operationTokens = _unitOfWork.OperationTokens.Search();
                _unitOfWork.OperationTokens.Remove(operationTokens.Where(x => x.UserId == userId));

                operationToken = _unitOfWork.OperationTokens.Insert(operationToken);
                await _unitOfWork.CommitAsync(cancellationToken);
                dbTransaction.Commit();

                return operationToken;
            }
        }
        

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<IList<OperationToken>>> SearchUserOperationTokensAsync(SearchOperationTokenModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get operation tokens.
            var operationTokens = GetOperationTokens(model);

            // Initialize search result.
            var loadOperationTokenResult = new SearchResult<IList<OperationToken>>();

            // Counting must be done.
            if (!model.IsTotalRecordIgnored)
                loadOperationTokenResult.Total = await operationTokens.CountAsync(cancellationToken);

            var pagination = model.Pagination;
            if (pagination != null)
                operationTokens = _baseRelationalDbService.Paginate(operationTokens, pagination);

            loadOperationTokenResult.Records = await operationTokens.ToListAsync(cancellationToken);
            return loadOperationTokenResult;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<OperationToken> SearchUserOperationTokenAsync(SearchOperationTokenModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get operation tokens.
            var operationTokens = GetOperationTokens(model);
            return await operationTokens.FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteOperationTokenUserIdsAsync(HashSet<int> userIds, CancellationToken cancellationToken)
        {
            var loadOperationTokenCondition = new SearchOperationTokenModel();
            loadOperationTokenCondition.UserIds = userIds;

            var operationTokens = GetOperationTokens(loadOperationTokenCondition);
            _unitOfWork.OperationTokens.Remove(operationTokens);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        /// <summary>
        /// Get user operation tokens using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        protected virtual IQueryable<OperationToken> GetOperationTokens(SearchOperationTokenModel condition)
        {
            // Get user operation tokens.
            var operationTokens = _unitOfWork.OperationTokens.Search();

            if (condition == null)
                return operationTokens;

            var userIds = condition.UserIds;
            if (userIds != null && userIds.Count > 0)
            {
                // Get valid user ids.
                userIds = userIds.Where(x => x > 0).ToHashSet();
                if (userIds != null && userIds.Count > 0)
                    operationTokens = operationTokens.Where(x => userIds.Contains(x.UserId));
            }

            var codes = condition.Codes;
            if (codes != null && codes.Count > 0)
            {
                codes = codes.Where(x => !string.IsNullOrWhiteSpace(x)).ToHashSet();
                if (codes != null && codes.Count > 0)
                    operationTokens = operationTokens.Where(x => codes.Contains(x.Code));
            }

            var issuedTime = condition.IssuedTime;
            if (issuedTime != null)
            {
                var from = issuedTime.From;
                var to = issuedTime.To;

                if (from != null)
                    operationTokens = operationTokens.Where(x => x.IssuedTime >= from);
                if (to != null)
                    operationTokens = operationTokens.Where(x => x.IssuedTime <= to);
            }

            var expiredTime = condition.ExpiredTime;
            if (expiredTime != null)
            {
                var from = expiredTime.From;
                var to = expiredTime.To;

                if (from != null)
                    operationTokens = operationTokens.Where(x => x.ExpiredTime != null && x.ExpiredTime >= from);

                if (to != null)
                    operationTokens = operationTokens.Where(x => x.ExpiredTime != null && x.ExpiredTime <= to);
            }

            return operationTokens;
        }

        #endregion
    }
}