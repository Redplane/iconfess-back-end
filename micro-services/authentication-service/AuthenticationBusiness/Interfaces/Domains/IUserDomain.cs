﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AuthenticationDb.Models.Entities;
using AuthenticationShared.ViewModels.User;
using ClientShared.Enumerations;
using ClientShared.Models;
using ClientShared.ViewModels;
using ServiceShared.Models.CachedEntities;
using SkiaSharp;

namespace AuthenticationBusiness.Interfaces.Domains
{
    public interface IUserDomain
    {
        #region Methods

        /// <summary>
        ///     Find user login information in system.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> LoginAsync(LoginViewModel model, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Exchange google identity with user information.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> GoogleLoginAsync(GoogleLoginViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Exchange facebook identity with user information.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> FacebookLoginAsync(FacebookLoginViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Search for users using specific conditions.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResult<IList<User>>> SearchUsersAsync(SearchUserViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Search for user using specific condition.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> SearchUserAsync(SearchUserViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Update password asynchronously.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> UpdateUserPasswordAsync(int userId, string hashedPassword,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add user access token to cache system.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task AddUserAccessTokenAsync(User user, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Find user access token by email asynchronously.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<AccessToken> FindUserAccessTokenAsync(string email,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Find user access token by email, hashed password asynchronously.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="hashedPassword"></param>
        /// <param name="userKind"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<AccessToken> FindUserAccessTokenAsync(string email, string hashedPassword, UserKind userKind,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add / update user signature.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="signature"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> AddUserSignatureAsync(int userId, string signature,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Upload user photo asynchronously.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="photo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> UploadUserPhotoAsync(int id, SKBitmap photo,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Find pending users and send them the activation email again.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RequestUserActivationTokenAsync(
            RequestUserActivationEmailViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Change user password asynchronously.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> ChangePasswordAsync(int id, ChangePasswordViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     From user information to generate user json web token.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        JwtViewModel GenerateJwt(User user);

        /// <summary>
        ///     Convert access token to jwt.
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        JwtViewModel ConvertAccessTokenToJwt(AccessToken accessToken);

        /// <summary>
        /// Register New User Basic
        /// </summary>
        /// <param name="basicUser"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task AddBasicUser(AddBasicUserViewModel basicUser,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}