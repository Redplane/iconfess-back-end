﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AuthenticationDb.Models.Entities;
using AuthenticationModel.Models;
using ClientShared.Models;

namespace AuthenticationBusiness.Interfaces.Domains
{
    public interface IOperationTokenDomain
    {
        #region Methods

        /// <summary>
        /// Add operation token asynchronously.
        /// </summary>
        /// <param name="operationToken"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<OperationToken> AddOperationTokenAsync(OperationToken operationToken, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Search user operation token using specific conditions.
        /// </summary>
        /// <returns></returns>
        Task<SearchResult<IList<OperationToken>>> SearchUserOperationTokensAsync(SearchOperationTokenModel model, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Search for operation token using specific condition.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<OperationToken> SearchUserOperationTokenAsync(SearchOperationTokenModel model, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Delete operation tokens by using user ids.
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task DeleteOperationTokenUserIdsAsync(HashSet<int> userIds, CancellationToken cancellationToken = default(CancellationToken));

        #endregion

    }
}