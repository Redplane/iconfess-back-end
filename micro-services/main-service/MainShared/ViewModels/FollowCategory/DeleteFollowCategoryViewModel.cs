﻿namespace MainShared.ViewModels.FollowCategory
{
    public class DeleteFollowCategoryViewModel
    {
        #region Properties

        public int CategoryId { get; set; }

        #endregion
    }
}