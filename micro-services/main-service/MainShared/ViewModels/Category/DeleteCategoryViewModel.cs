﻿namespace MainShared.ViewModels.Category
{
    public class DeleteCategoryViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of category
        /// </summary>
        public int Id { get; set; }

        #endregion
    }
}