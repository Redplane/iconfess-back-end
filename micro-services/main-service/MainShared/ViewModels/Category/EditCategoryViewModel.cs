﻿namespace MainShared.ViewModels.Category
{
    public class EditCategoryViewModel
    {
        #region Properties

        /// <summary>
        ///     Name of category
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Description of category
        /// </summary>
        public string Description { get; set; }

        #endregion
    }
}