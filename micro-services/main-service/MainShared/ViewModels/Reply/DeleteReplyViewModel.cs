﻿namespace MainShared.ViewModels.Reply
{
    public class DeleteReplyViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of reply
        /// </summary>
        public int Id { get; set; }

        #endregion
    }
}