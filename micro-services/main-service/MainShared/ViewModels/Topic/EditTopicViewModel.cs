﻿namespace MainShared.ViewModels.Topic
{
    public class EditTopicViewModel
    {
        #region Properties

        /// <summary>
        ///     Title of topic
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Body of topic
        /// </summary>
        public string Body { get; set; }

        #endregion
    }
}