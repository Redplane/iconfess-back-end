﻿namespace MainShared.ViewModels.Topic
{
    public class DeleteTopicViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of topic
        /// </summary>
        public int Id { get; set; }

        #endregion
    }
}