﻿using MainBusiness.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using ServiceShared.Models;
using ServiceShared.Services;
using ServiceShared.ViewModels;

namespace MainBusiness.Services
{
    public class AppProfileService : BaseProfileService, IAppProfileService
    {
        #region Constructor

        public AppProfileService(IOptions<AppJwtModel> appJwt, IHttpContextAccessor httpContextAccessor) : base(appJwt,
            httpContextAccessor)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual UserViewModel GetProfile()
        {
            return GetProfile<UserViewModel>();
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="user"></param>
        public virtual void SetProfile(UserViewModel user)
        {
            SetProfile<UserViewModel>(user);
        }

        #endregion
    }
}