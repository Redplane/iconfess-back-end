﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ClientShared.Enumerations;
using ClientShared.Resources;
using MainBusiness.Interfaces;
using MainBusiness.Interfaces.Domains;
using MainBusiness.Interfaces.Services;
using MainBusiness.Models.NotificationMessages;
using MainDb.Models.Entities;
using MainMicroService.Constants;
using MainMicroService.Interfaces.Services;
using MainMicroService.Models.AdditionalMessageInfo.Topic;
using MainShared.Resources;
using MainShared.ViewModels.Topic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceShared.Authentications.ActionFilters;
using ServiceShared.Constants;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MainMicroService.Controllers
{
    [Route("api/[controller]")]
    public class TopicController : Controller
    {
        #region Constructors

        /// <summary>
        /// </summary>
        public TopicController(
            IBaseTimeService baseTimeService,
            IBaseRelationalDbService relationalDbService,
            IBaseEncryptionService encryptionService,
            IAppProfileService identityService,
            ISendMailService sendMailService,
            IEmailCacheService emailCacheService,
            ILogger<TopicController> logger,
            ITopicDomain topicDomain,
            IMapper mapper,
            IAppProfileService appProfileService,
            IFollowCategoryDomain followCategoryDomain,
            IInternalMqService internalMqService)
        {
            _sendMailService = sendMailService;
            _emailCacheService = emailCacheService;
            _logger = logger;
            _topicDomain = topicDomain;
            _mapper = mapper;
            _appProfileService = appProfileService;
            _followCategoryDomain = followCategoryDomain;
            _internalMqService = internalMqService;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Send email service
        /// </summary>
        private readonly ISendMailService _sendMailService;

        /// <summary>
        ///     Email cache service.
        /// </summary>
        private readonly IEmailCacheService _emailCacheService;

        /// <summary>
        ///     Logging instance.
        /// </summary>
        private readonly ILogger _logger;

        private readonly IMapper _mapper;

        private readonly ITopicDomain _topicDomain;

        private readonly IFollowCategoryDomain _followCategoryDomain;

        private readonly IAppProfileService _appProfileService;

        private readonly IInternalMqService _internalMqService;

        #endregion

        #region Methods

        /// <summary>
        ///     Add topic to system.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<IActionResult> AddTopic([FromBody] AddTopicViewModel model)
        {
            if (model == null)
            {
                model = new AddTopicViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Add topic.
            var topic = await _topicDomain.AddTopicAsync(model, CancellationToken.None);
            var clonedTopic = _mapper.Map<Topic>(topic);
            clonedTopic.Body = null;

            // Fire real-time message.
            // Get topic followers.
            var categoryFollowerIds = await _followCategoryDomain
                .GetCategoryFollowerIdsAsync(topic.CategoryId);

            // Build recipient ids.
            var recipientIds = categoryFollowerIds.ToHashSet();
            // Send internal mq asynchronously.
            await _internalMqService.SendInternalMqAsync(recipientIds, null, new[] { MqChannelNameConstants.MainServiceChannelName },
                MqEventNameConstants.SendMessageFromMainToRealTime, RealTimeMessages.AddTopic, clonedTopic, true);
            
            return Ok(topic);
        }

        /// <summary>
        ///     Edit topic by using specific information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditTopic([FromRoute] int id, [FromBody] EditTopicViewModel model)
        {
            #region Parameters validation

            if (model == null)
            {
                model = new EditTopicViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            // Update topic information.
            var topic = await _topicDomain.EditTopicAsync(id, model);

            // Fire real-time message.
            // Get topic followers.
            var categoryFollowerIds = await _followCategoryDomain
                .GetCategoryFollowerIdsAsync(topic.CategoryId);

            // Build recipient ids.
            var recipientIds = categoryFollowerIds.ToHashSet();

            // Send internal mq asynchronously.
            await _internalMqService.SendInternalMqAsync(recipientIds, null, new[] { MqChannelNameConstants.MainServiceChannelName },
                MqEventNameConstants.SendMessageFromMainToRealTime, RealTimeMessages.EditTopic, topic, true);

            if (topic.Status != ItemStatus.Disabled)
                return Ok(topic);

            //var users = _unitOfWork.Accounts.Search();
            //users = users.Where(x => x.Id == topic.OwnerId);
            //var user = await users.FirstOrDefaultAsync();

            //if (user != null)
            //{
            //    var emailTemplate = _emailCacheService.Read(EmailTemplateConstant.DeleteTopic);
            //    if (emailTemplate != null)
            //    {
            //        await _sendMailService.SendAsync(new HashSet<string> {user.Email}, null, null,
            //            emailTemplate.Subject,
            //            emailTemplate.Content, true, CancellationToken.None);

            //        _logger.LogInformation($"Sent message to {user.Email} with subject {emailTemplate.Subject}");
            //    }
            //}

            //#region Notification

            //// Get requester profile.
            //var profile = _appProfileService.GetProfile();

            //// Search for follow topic.
            //var followTopics = _unitOfWork.FollowingTopics.Search();

            //followTopics = followTopics.Where(x => x.TopicId == id);

            //// Get all topic follower
            //var topicFollowers = followTopics.Select(x => x.FollowerId);

            //// Search for all reply
            //var replies = _unitOfWork.Replies.Search();

            //replies = replies.Where(x => x.TopicId == id);

            //var topicRepliers = replies.Select(x => x.OwnerId);

            //var followerIds = new HashSet<int>();

            //foreach (var topicFollower in topicFollowers)
            //    followerIds.Add(topicFollower);

            //foreach (var topicReplier in topicRepliers)
            //    followerIds.Add(topicReplier);

            //var additionalInfo = new AddTopicAdditionalInfoModel();
            //additionalInfo.TopicName = model.Title;
            //additionalInfo.CreatorName = profile.Nickname;
            //await _notificationMessageDomain.AddNotificationMessageToListUser(
            //    MqEventNameConstants.AddedTopic,
            //    new AddListUserNotificationMessageModel<AddTopicAdditionalInfoModel>(followerIds, additionalInfo,
            //        NotificationMessages.SomeoneCreatedTopic));

            //#endregion

            return Ok(topic);
        }

        /// <summary>
        ///     Delete a topic.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("")]
        [Authorize(Policy = PolicyConstant.IsAdminPolicy)]
        public async Task<IActionResult> DeleteTopic([FromRoute] int id)
        {
            var deleteTopicViewModel = new DeleteTopicViewModel
            {
                Id = id
            };

            await _topicDomain.DeleteTopicAsync(deleteTopicViewModel);

            var topic = await _topicDomain.GetTopicUsingIdAsync(id, CancellationToken.None);

            #region Send email 

            if (topic == null)
                return NotFound(new ApiResponse(HttpMessages.TopicNotFound));

            //var users = _unitOfWork.Accounts.Search();

            //users = users.Where(x => x.Id == topic.OwnerId);

            //var user = await users.FirstOrDefaultAsync();

            //if (user != null)
            //{
            //    var emailTemplate = _emailCacheService.Read(EmailTemplateConstant.DeleteTopic);
            //    if (emailTemplate != null)
            //    {
            //        await _sendMailService.SendAsync(new HashSet<string> {user.Email}, null, null,
            //            emailTemplate.Subject,
            //            emailTemplate.Content, true, CancellationToken.None);

            //        _logger.LogInformation($"Sent message to {user.Email} with subject {emailTemplate.Subject}");
            //    }
            //}

            #endregion

            //#region Real-time message broadcast

            //// Send real-time message to all admins.
            //var broadcastRealTimeMessageTask = _realTimeService.SendRealTimeMessageToGroupsAsync(
            //    new[] {RealTimeGroupConstant.Admin}, RealTimeEventConstant.DeleteTopic, topic,
            //    CancellationToken.None);

            //// Send push notification to all admin.
            //var collapseKey = Guid.NewGuid().ToString("D");
            //var realTimeMessage = new RealTimeMessage<Topic>();
            //realTimeMessage.Title = RealTimeMessages.DeleteTopicTitle;
            //realTimeMessage.Body = RealTimeMessages.DeleteTopicContent;
            //realTimeMessage.ExtraInfo = topic;

            //var broadcastPushMessageTask = _realTimeService.SendPushMessageToGroupsAsync(
            //    new[] {RealTimeGroupConstant.Admin}, collapseKey, realTimeMessage);

            //await Task.WhenAll(broadcastRealTimeMessageTask, broadcastPushMessageTask);

            //#endregion


            return Ok();
        }

        /// <summary>
        ///     Load topic by using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [ByPassAuthorization]
        public async Task<IActionResult> LoadTopics([FromBody] SearchTopicViewModel condition)
        {
            #region Parameters validation

            if (condition == null)
            {
                condition = new SearchTopicViewModel();
                TryValidateModel(condition);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            var loadTopicsResult = await _topicDomain.SearchTopicsAsync(condition, CancellationToken.None);
            return Ok(loadTopicsResult);
        }

        #endregion
    }
}