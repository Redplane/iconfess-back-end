﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MainBusiness.Interfaces;
using MainBusiness.Interfaces.Domains;
using MainBusiness.Models.NotificationMessages;
using MainDb.Interfaces;
using MainDb.Models.Entities;
using MainMicroService.Constants;
using MainMicroService.Models.AdditionalMessageInfo.CategoryGroup;
using MainModel.Enumerations;
using MainShared.Resources;
using MainShared.ViewModels.CategoryGroup;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ServiceShared.Authentications.ActionFilters;
using ServiceShared.Exceptions;
using ServiceShared.Interfaces.Services;

namespace MainMicroService.Controllers
{
    [Route("api/category-group")]
    public class CategoryGroupController : ApiBaseController
    {
        #region Constructures

        public CategoryGroupController(
            IAppUnitOfWork unitOfWork,
            IMapper mapper,
            IBaseTimeService baseTimeService,
            IBaseRelationalDbService relationalDbService,
            IBaseEncryptionService encryptionService,
            IAppProfileService profileService,
            ICategoryGroupDomain categoryGroupService,
            IAppProfileService appProfileService) : base(
            unitOfWork, mapper, baseTimeService,
            relationalDbService, profileService)
        {
            _categoryGroupService = categoryGroupService;
            _mapper = mapper;
            _appProfileService = appProfileService;
        }

        #endregion

        #region Properties
        
        private readonly ICategoryGroupDomain _categoryGroupService;

        private readonly IMapper _mapper;
        
        private readonly IAppProfileService _appProfileService;

        #endregion

        #region Methods

        /// <summary>
        ///     Add category group to system.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("")]
        [Authorize(Policy = PolicyConstant.IsAdminPolicy)]
        public async Task<IActionResult> AddCategoryGroup([FromBody] AddCategoryGroupViewModel model)
        {
            #region Parameters validation

            if (model == null)
            {
                model = new AddCategoryGroupViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            var categoryGroup = await _categoryGroupService.AddCategoryGroup(model);

            // Get requester profile.
            var profile = _appProfileService.GetProfile();

            //#region Real-time message broadcast

            //// Send real-time message to all admins.
            //var broadcastRealTimeMessageTask = _realTimeService.SendRealTimeMessageToGroupsAsync(
            //    new[] {RealTimeGroupConstant.Admin}, RealTimeEventConstant.AddCategoryGroup, categoryGroup,
            //    CancellationToken.None);

            //// Send push notification to all admin.
            //var collapseKey = Guid.NewGuid().ToString("D");
            //var realTimeMessage = new RealTimeMessage<CategoryGroup>();
            //realTimeMessage.Title = RealTimeMessages.AddNewCategoryGroupTitle;
            //realTimeMessage.Body = RealTimeMessages.AddNewCategoryGroupContent;
            //realTimeMessage.ExtraInfo = categoryGroup;

            //var broadcastPushMessageTask = _realTimeService.SendPushMessageToGroupsAsync(
            //    new[] {RealTimeGroupConstant.Admin}, collapseKey, realTimeMessage);

            //await Task.WhenAll(broadcastRealTimeMessageTask, broadcastPushMessageTask);

            //#endregion

            #region Notification

            var additionalInfo = new AddCategoryGroupAdditionalInfoModel();
            additionalInfo.CategoryGroupName = model.Name;
            additionalInfo.CreatorName = profile.Nickname;

            var ignoreUsers = new HashSet<int> {profile.Id};

            //await _notificationMessageDomain.AddNotificationMessageToUserGroup(UserGroup.Admin,
            //    new AddUserGroupNotificationMessageModel<AddCategoryGroupAdditionalInfoModel>(additionalInfo,
            //        NotificationMessages.SomeoneCreatedCategoryGroup, ignoreUsers));

            #endregion

            return Ok(categoryGroup);
        }

        /// <summary>
        ///     Edit category group by using specific information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Policy = PolicyConstant.IsAdminPolicy)]
        public async Task<IActionResult> EditCategoryGroup([FromRoute] int id,
            [FromBody] EditCategoryGroupViewModel model)
        {
            #region Parameters validation

            if (model == null)
            {
                model = new EditCategoryGroupViewModel();
                TryValidateModel(model);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            #region Update category group. information

            try
            {
                var categoryGroup = await _categoryGroupService.EditCategoryGroup(id, model);


                var profile = _appProfileService.GetProfile();

                //// Send real-time message to all admins.
                //var broadcastRealTimeMessageTask = _realTimeService.SendRealTimeMessageToGroupsAsync(
                //    new[] {RealTimeGroupConstant.Admin}, RealTimeEventConstant.EditCategoryGroup, categoryGroup,
                //    CancellationToken.None);

                //// Send push notification to all admin.
                //var collapseKey = Guid.NewGuid().ToString("D");
                //var realTimeMessage = new RealTimeMessage<CategoryGroup>();
                //realTimeMessage.Title = RealTimeMessages.EditCategoryGroupTitle;
                //realTimeMessage.Body = RealTimeMessages.EditCategoryGroupContent;
                //realTimeMessage.ExtraInfo = categoryGroup;

                //var broadcastPushMessageTask = _realTimeService.SendPushMessageToGroupsAsync(
                //    new[] {RealTimeGroupConstant.Admin}, collapseKey, realTimeMessage);

                //await Task.WhenAll(broadcastRealTimeMessageTask, broadcastPushMessageTask);

                #region Notification

                var additionalInfo = new EditCategoryGroupAdditionalInfoModel();
                additionalInfo.CategoryGroupName = model.Name;
                additionalInfo.EditorName = profile.Nickname;

                var ignoreUsers = new HashSet<int> {profile.Id};

                //await _notificationMessageDomain.AddNotificationMessageToUserGroup(UserGroup.Admin,
                //    new AddUserGroupNotificationMessageModel<EditCategoryGroupAdditionalInfoModel>(additionalInfo,
                //        NotificationMessages.SomeoneEditedCategoryGroup, ignoreUsers));

                #endregion

                return Ok(categoryGroup);
            }
            catch (Exception exception)
            {
                if (!(exception is NotModifiedException))
                    return Ok();

                throw;
            }

            #endregion
        }

        /// <summary>
        ///     Load category group by using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [ByPassAuthorization]
        public async Task<IActionResult> LoadCategoryGroups([FromBody] SearchCategoryGroupViewModel condition)
        {
            #region Parameters validation

            if (condition == null)
            {
                condition = new SearchCategoryGroupViewModel();
                TryValidateModel(condition);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            #endregion

            var loadCategoryGroupsResult = await _categoryGroupService.SearchCategoryGroupsAsync(condition);
            return Ok(loadCategoryGroupsResult);
        }

        #endregion
    }
}