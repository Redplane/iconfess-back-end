﻿using ServiceShared.Services;
using ServiceShared.ViewModels;

namespace MainMicroService.Services
{
    public class ProfileCacheService : BaseKeyValueCacheService<int, UserViewModel>
    {
    }
}