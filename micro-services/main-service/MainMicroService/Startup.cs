﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ClientShared.Enumerations;
using MainBusiness.Domain;
using MainBusiness.Interfaces;
using MainBusiness.Interfaces.Domains;
using MainBusiness.Interfaces.Services;
using MainBusiness.Services;
using MainDb.Interfaces;
using MainDb.Models.Contexts;
using MainDb.Services;
using MainMicroService.Authentications.Handlers;
using MainMicroService.Authentications.Requirements;
using MainMicroService.Configs;
using MainMicroService.Constants;
using MainMicroService.Interfaces.Services;
using MainMicroService.Models;
using MainMicroService.Models.Captcha;
using MainMicroService.Services;
using MainModel.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Routing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.SwaggerGeneration.Processors.Security;
using Serilog;
using ServiceShared.Extensions;
using ServiceShared.Interfaces.Services;
using ServiceShared.Models;
using ServiceShared.Services;
using ServiceShared.ViewModels;
using VgySdk.Interfaces;
using VgySdk.Service;

namespace MainMicroService
{
    public class Startup
    {
        #region Properties

        /// <summary>
        ///     Instance stores configuration of application.
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        ///     Hosting environement configuration.
        /// </summary>
        public IHostingEnvironment HostingEnvironment { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Callback which is fired when application starts.
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddJsonFile($"dbSetting.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            // Set hosting environment.
            HostingEnvironment = env;
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add services DI to app.
            AddServices(services);

            // Load jwt configuration from setting files.
            services.Configure<AppJwtModel>(Configuration.GetSection(MainConfigKeyConstant.AppJwt));
            services.Configure<ApplicationSetting>(Configuration.GetSection(nameof(ApplicationSetting)));
            //services.Configure<FcmOption>(Configuration.GetSection(MainConfigKeyConstant.AppFirebase));
            services.Configure<SendGridSetting>(Configuration.GetSection(MainConfigKeyConstant.AppSendGrid));
            //services.Configure<PusherSetting>(Configuration.GetSection(nameof(PusherSetting)));
            services.Configure<CaptchaSetting>(Configuration.GetSection(nameof(CaptchaSetting)));

            // Build a service provider.
            //var fcmOption = servicesProvider.GetService<IOptions<FcmOption>>().Value;

            //#if DEBUG
            //            var dbContext = servicesProvider.GetService<DbContext>();
            //            var sqlReader = dbContext.Database.ExecuteSqlQuery("select sqlite_version();");
            //            sqlReader.Read();
            //            var value = sqlReader.DbDataReader[0];
            //#endif

            // Cors configuration.
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.WithExposedHeaders("WWW-Authenticate");
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowCredentials();

            // Add cors configuration to service configuration.
            services.AddCors(options => { options.AddPolicy("AllowAll", corsBuilder.Build()); });
            services.AddOptions();

            // Register jwt service.
            JsonWebTokenConfigs.Register(Configuration, services);

            // Implement cache configuration.
            CacheClientConfigs.Register(Configuration, services);

            // Register real-time clients.
            MessageQueueConfigs.Register(Configuration, services);

            // Add automaper configuration.
            services.AddAutoMapper(options => options.AddProfile(typeof(MappingProfile)));
            
            services.AddHttpClient();

            // Add swagger.
            services.AddSwaggerDocument();
           
            services.AddAuthorization(x => x.AddPolicy(PolicyConstant.IsAdminPolicy,
                builder => { builder.AddRequirements(new RoleRequirement(new[] {UserRole.Admin})); }));
            
            #region Mvc builder

            // Construct mvc options.
            services.AddMvc(mvcOptions =>
                {
                    ////only allow authenticated users
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
#if !ALLOW_ANONYMOUS
                        .AddRequirements(new SolidAccountRequirement())
#endif
                        .Build();

                    mvcOptions.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            #endregion
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="serviceProvider"></param>
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            // Enable logging.
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            // Use in-app exception handler.
            app.UseCustomizedExceptionHandler(env);

            // Use strict transport security.
            app.UseHsts();

            // Use JWT Bearer authentication in the system.
            app.UseAuthentication();

            // Use https redirection.
            //app.UseHttpsRedirection();

            // Enable cors.
            app.UseCors("AllowAll");

            app.UseSwagger();
            app.UseSwaggerUi3();
            
            // Enable MVC features.
            app.UseMvc();
        }

        /// <summary>
        ///     Add dependency injection of services to app.
        /// </summary>
        private void AddServices(IServiceCollection services)
        {
            // Add entity framework to services collection.
            var sqlConnection = "";

#if USE_SQLITE
            sqlConnection = Configuration.GetConnectionString(AppConfigKeyConstant.SqliteConnectionString);
            services.AddDbContext<RelationalDbContext>(
                options => options.UseSqlite(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_SQLITE_INMEMORY
            sqlConnection = Configuration.GetConnectionString(MainConfigKeyConstant.SqliteConnectionString);
            //services.AddDbContext<RelationalDbContext>(
            //    options => options.UseSqlite(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
            //services.AddScoped<DbContext, RelationalDbContext>();

            var sandboxConnection = new SqliteConnection("Data Source=:memory:");
            sandboxConnection.Open();

            using (var physicalConnection = new SqliteConnection(sqlConnection))
            {
                physicalConnection.Open();
                physicalConnection.BackupDatabase(sandboxConnection);
            }

            services.AddDbContext<RelationalDbContext>(
                options => options.UseSqlite(sandboxConnection, b => b.MigrationsAssembly(nameof(MainMicroService)))
                    .EnableSensitiveDataLogging());
#elif USE_AZURE_SQL
            sqlConnection = Configuration.GetConnectionString("azureSqlServerConnectionString");
            services.AddDbContext<RelationalDatabaseContext>(
                options => options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#elif USE_IN_MEMORY
            services.AddDbContext<InMemoryRelationalDbContext>(
                options => options.UseInMemoryDatabase("iConfess")
                    .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning)));

            var bDbCreated = false;
            services.AddScoped<DbContext>(context =>
            {
                var inMemoryContext = context.GetService<InMemoryRelationalDbContext>();
                if (!bDbCreated)
                {
                    inMemoryContext.Database.EnsureCreated();
                    bDbCreated = true;
                }
                return context.GetService<InMemoryRelationalDbContext>();
            });
#else
            sqlConnection = Configuration.GetConnectionString("sqlServerConnectionString");
            services.AddDbContext<RelationalDbContext>(options => options.UseSqlServer(sqlConnection, b => b.MigrationsAssembly(nameof(Main))));
#endif

            // Add scoped.
            services.AddScoped<DbContext, RelationalDbContext>();

            // Injections configuration.
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IAppUnitOfWork, AppUnitOfWork>();
            services.AddScoped<IBaseRelationalDbService, BaseRelationalDbService>();

            services.AddScoped<IBaseEncryptionService, EncryptionService>();
            services.AddScoped<IAppProfileService, AppProfileService>();
            services.AddScoped<IBaseTimeService, BaseTimeService>();
            //services.AddScoped<ICloudMessagingService, FcmService>();
            //            services.AddScoped<INotifyService, NotifyService>();
            services.AddScoped<ISendMailService, SendGridService>();
            services.AddScoped<IMustacheService, MustacheService>();
            services.AddScoped<IExternalAuthenticationService, ExternalAuthenticationService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddScoped<IPusherService, PusherService>();
            services.AddScoped<ICaptchaService, CaptchaService>();
            services.AddScoped<IAppProfileCacheService, AppProfileCacheService>();

            // Store user information in cache
            services.AddSingleton<IBaseKeyValueCacheService<int, UserViewModel>, ProfileCacheService>();
            //services.AddSingleton<IRealTimeConnectionCacheService, RealTimeConnectionCacheService>();

            // Initialize real-time notification service as single instance.
            //services.AddSingleton<IRealTimeService, RealTimeService>();

            // Initialize vgy service.
            services.AddScoped<IVgyService, VgyService>();

            // Requirement handler.
            services.AddScoped<IAuthorizationHandler, SolidAccountRequirementHandler>();
            services.AddScoped<IAuthorizationHandler, RoleRequirementHandler>();

            services.AddScoped<ITopicDomain, TopicDomain>();
            services.AddScoped<ICategoryDomain, CategoryDomain>();
            services.AddScoped<ICategoryGroupDomain, CategoryGroupDomain>();
            services.AddScoped<ITopicReportDomain, TopicReportDomain>();
            services.AddScoped<IReplyDomain, TopicReplyDomain>();
            services.AddScoped<IFollowTopicDomain, FollowTopicDomain>();
            services.AddScoped<IFollowCategoryDomain, FollowCategoryDomain>();
            services.AddScoped<IInternalMqService, InternalMqService>();


            // Add redis configuration.
            //SqlCache.Register(Configuration, services);

            // Get email cache option.
            var emailCacheOption = (Dictionary<string, EmailCacheOption>) Configuration.GetSection("emailCache")
                .Get(typeof(Dictionary<string, EmailCacheOption>));
            var emailCacheService = new EmailCacheService();
            emailCacheService.HostingEnvironment = HostingEnvironment;
            emailCacheService.ReadConfiguration(emailCacheOption);
            services.AddSingleton<IEmailCacheService>(emailCacheService);

            services.Configure<RouteOptions>(options => { options.LowercaseUrls = true; });

            // Add Swagger API document.
            services.AddSwaggerDocument(settings =>
            {
                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                settings.SerializerSettings = jsonSerializerSettings;

                settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("API Key"));
                settings.DocumentProcessors.Add(new SecurityDefinitionAppender("API Key",
                    new SwaggerSecurityScheme
                    {
                        Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        Description = "Copy 'Bearer ' + valid JWT token into field",
                        In = SwaggerSecurityApiKeyLocation.Header
                    }));
            });
        }

        #endregion
    }
}