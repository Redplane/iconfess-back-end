﻿namespace MainMicroService.Constants
{
    public class MainModuleRedisKeyConstant
    {
        #region Constructor

        /// <summary>
        ///     Profile redis client.
        /// </summary>
        public const string ProfileRedisClient = "profileRedisClient";

        #endregion
    }
}