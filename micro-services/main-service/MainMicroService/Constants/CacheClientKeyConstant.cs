﻿namespace MainMicroService.Constants
{
    public class CacheClientKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Profile redis client.
        /// </summary>
        public const string AccessTokenSqlClient = "accessTokenSqlCacheClient";
        
        #endregion
    }
}