﻿using System.Collections.Generic;
using MainMicroService.Constants;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceShared.Constants;
using ServiceShared.Interfaces;
using ServiceShared.Models.CachedEntities;
using ServiceShared.Services;
using ServiceStack.OrmLite;
using MainConfigKeyConstant = MainMicroService.Constants.MainConfigKeyConstant;

namespace MainMicroService.Configs
{
    public class CacheClientConfigs
    {
        /// <summary>
        ///     Register redis configuration for caching.
        /// </summary>
        public static void Register(IConfiguration configuration, IServiceCollection serviceCollection)
        {
            // Get redis configuration.

            var redisClients = new Dictionary<string, string>();
            configuration
                .GetSection(MainConfigKeyConstant.CacheClients)
                .Bind(redisClients);

            // Get redis configuration.
            var cacheClients = new Dictionary<string, string>();
            configuration
                .GetSection(MainConfigKeyConstant.CacheClients)
                .Bind(cacheClients);

            var accessTokenCacheInstance = new OrmLiteConnectionFactory(cacheClients[CacheClientKeyConstant.AccessTokenSqlClient], SqlServer2017Dialect.Provider);
            var accessTokenDbConnection = new AccessTokenDbConnection(accessTokenCacheInstance);
            accessTokenDbConnection.Open();
            accessTokenDbConnection.CreateTableIfNotExists<AccessToken>();

            // Add redis client manager cache service.
            serviceCollection.AddSingleton<IAccessTokenDbConnection, AccessTokenDbConnection>(x => accessTokenDbConnection);


        }
    }
}