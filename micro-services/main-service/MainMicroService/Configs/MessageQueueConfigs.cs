﻿using System.Collections.Generic;
using MainBusiness.Services.Pushers;
using MainMicroService.Constants;
using MainMicroService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PusherServer;
using ServiceShared.Constants;
using ServiceShared.Models;

namespace MainMicroService.Configs
{
    public class MessageQueueConfigs
    {
        #region Methods

        /// <summary>
        /// Register message queues channel.
        /// </summary>
        public static void Register(IConfiguration configuration, IServiceCollection services)
        {
            // Get pusher client options.
            var pusherClientOptionModels = new Dictionary<string, PusherClientOptionModel>();
            configuration
                .GetSection(MainConfigKeyConstant.PusherRealTimeChannels)
                .Bind(pusherClientOptionModels);

            // Get pusher configuration.
            var messageBroadcasterModel = pusherClientOptionModels[MqClientConstants.MesasgeBroadcaster];

            // Message broadcaster.
            var options = new PusherOptions();
            options.JsonSerializer = new CamelCasedPusherSerializer();
            options.Cluster = messageBroadcasterModel.Cluster;
            options.Encrypted = messageBroadcasterModel.Encrypted;
            
            var messageBroadcasterClient = new MessageBroadcasterPusherClient(
                messageBroadcasterModel.AppId,
                messageBroadcasterModel.Key, 
                messageBroadcasterModel.Secret,
                options);

            services.AddScoped(x => messageBroadcasterClient);
        }

        #endregion
    }
}