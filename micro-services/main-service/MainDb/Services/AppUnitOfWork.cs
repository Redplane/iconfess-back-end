﻿using MainDb.Interfaces;
using MainDb.Models.Entities;
using Microsoft.EntityFrameworkCore;
using ServiceShared.Interfaces.Services;
using ServiceShared.Services;

namespace MainDb.Services
{
    public class AppUnitOfWork : BaseUnitOfWork, IAppUnitOfWork
    {
        #region Constructors

        /// <summary>
        ///     Initiate unit of work with database context provided by Entity Framework.
        /// </summary>
        public AppUnitOfWork(DbContext dbContext,
            IBaseRepository<CategoryGroup> categoryGroups,
            IBaseRepository<Category> categories, IBaseRepository<FollowCategory> followingCategories,
            IBaseRepository<Reply> replies, IBaseRepository<Topic> topics, IBaseRepository<FollowTopic> followingTopics,
            IBaseRepository<ReportTopic> reportTopics,
            IBaseRepository<CategorySummary> categorySummaries,
            IBaseRepository<TopicSummary> topicSummaries) : base(dbContext)
        {
            CategoryGroups = categoryGroups;
            Categories = categories;
            FollowingCategories = followingCategories;
            Replies = replies;
            Topics = topics;
            FollowingTopics = followingTopics;
            ReportTopics = reportTopics;
            CategorySummaries = categorySummaries;
            TopicSummaries = topicSummaries;
        }

        #endregion

        #region Properties

        public IBaseRepository<CategoryGroup> CategoryGroups { get; }

        public IBaseRepository<Category> Categories { get; }

        public IBaseRepository<FollowCategory> FollowingCategories { get; }

        public IBaseRepository<Reply> Replies { get; }

        public IBaseRepository<Topic> Topics { get; }

        public IBaseRepository<FollowTopic> FollowingTopics { get; }

        public IBaseRepository<ReportTopic> ReportTopics { get; }
        
        public IBaseRepository<CategorySummary> CategorySummaries { get; }

        public IBaseRepository<TopicSummary> TopicSummaries { get; }

        #endregion
    }
}