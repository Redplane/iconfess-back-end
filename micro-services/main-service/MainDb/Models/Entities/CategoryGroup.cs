﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClientShared.Enumerations;
using Newtonsoft.Json;

namespace MainDb.Models.Entities
{
    public class CategoryGroup
    {
        #region Properties

        /// <summary>
        ///     Id of category group.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        ///     Who created the current category group.
        /// </summary>
        [Required]
        public int CreatorId { get; set; }

        /// <summary>
        ///     Name of category group.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        ///     Description of category group.
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        ///     Status of category group.
        /// </summary>
        public ItemStatus Status { get; set; }

        /// <summary>
        ///     When the category group was created
        /// </summary>
        [Required]
        public double CreatedTime { get; set; }

        /// <summary>
        ///     When the category group was lastly modified.
        /// </summary>
        public double? LastModifiedTime { get; set; }

        #endregion

        #region Relationships

        /// <summary>
        ///     List of category which are related to the current category group.
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Category> Categories { get; set; }

        /// <summary>
        ///     Topics which this category group contains.
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Topic> Topics { get; set; }

        #endregion

        #region Constructors

        public CategoryGroup()
        {
        }

        public CategoryGroup(int id, int creatorId, string name, string description, ItemStatus status,
            double createdTime, double? lastModifiedTime)
        {
            Id = id;
            CreatorId = creatorId;
            Name = name;
            Description = description;
            Status = status;
            CreatedTime = createdTime;
            LastModifiedTime = lastModifiedTime;
        }

        #endregion
    }
}