﻿using System.Linq;
using System.Threading.Tasks;
using MainDb.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace MainDb.Models.Contexts
{
    public class RelationalDbContext : DbContext
    {
        #region Constructors

        /// <summary>
        ///     Initiate database context with connection string.
        /// </summary>
        public RelationalDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        ///     List of category groups in database.
        /// </summary>
        public virtual DbSet<CategoryGroup> CategoryGroups { get; set; }

        /// <summary>
        ///     List of categories in database.
        /// </summary>
        public virtual DbSet<Category> Categories { get; set; }

        /// <summary>
        ///     List of relationships between followers and categories. (many - many)
        /// </summary>
        public virtual DbSet<FollowCategory> FollowCategories { get; set; }

        /// <summary>
        ///     List of relationships between followers and posts (many - many).
        /// </summary>
        public virtual DbSet<FollowTopic> FollowTopics { get; set; }

        /// <summary>
        ///     List of posts.
        /// </summary>
        public virtual DbSet<Topic> Topics { get; set; }

        /// <summary>
        ///     List of post reports.
        /// </summary>
        public virtual DbSet<ReportTopic> ReportTopics { get; set; }
        
        /// <summary>
        ///     Topic replies.
        /// </summary>
        public virtual DbSet<Reply> Replies { get; set; }
        
        /// <summary>
        ///     Category summaries.
        /// </summary>
        public virtual DbSet<CategorySummary> CategorySummaries { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Save changes into database.
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            return SaveChanges();
        }

        /// <summary>
        ///     Save changes into database asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<int> CommitAsync()
        {
            return await SaveChangesAsync();
        }

        /// <summary>
        ///     Callback which is fired when model is being created.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Category group table.
            AddCategoryGroupTable(modelBuilder);

            // Category table.
            AddCategoryTable(modelBuilder);

            // Topic table.
            AddTopicTable(modelBuilder);

            // Reply table.
            AddReplyTable(modelBuilder);

            // Follow category table.
            AddFollowCategoryTable(modelBuilder);

            // Follow topic table.
            AddFollowTopicTable(modelBuilder);

            // Report topic table.
            AddReportTopicTable(modelBuilder);
            
            // Add category summary table.
            AddCategorySummaryTable(modelBuilder);

            AddTopicSummaryTable(modelBuilder);

            // Use model builder to specify composite primary keys.
            // Composite primary keys configuration

            // This is for remove pluralization naming convention in database defined by Entity Framework.
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
                entity.Relational().TableName = entity.DisplayName();

            // Disable cascade delete.
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
        }

        #region Tables initialization

        /// <summary>
        ///     Initialize category table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddCategoryGroupTable(ModelBuilder modelBuilder)
        {
            var categoryGroup = modelBuilder.Entity<CategoryGroup>();

            // Set primary key.
            categoryGroup.HasKey(x => x.Id);
            categoryGroup.Property(x => x.Id).UseSqlServerIdentityColumn();
        }

        /// <summary>
        ///     Initialize category table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddCategoryTable(ModelBuilder modelBuilder)
        {
            var category = modelBuilder.Entity<Category>();

            // Set primary key.
            category.HasKey(x => x.Id);
            category.Property(x => x.Id).UseSqlServerIdentityColumn();

            // Relationship between category & category group.
            category.HasOne(x => x.CategoryGroup).WithMany(x => x.Categories).HasForeignKey(x => x.CategoryGroupId);
        }

        /// <summary>
        ///     Initialize topic table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddTopicTable(ModelBuilder modelBuilder)
        {
            // Find topic instance.
            var topic = modelBuilder.Entity<Topic>();

            // Primary key initialization.
            topic.HasKey(x => x.Id);
            topic.Property(x => x.Id).UseSqlServerIdentityColumn();

            // Relationship between topic and category.
            topic.HasOne(x => x.Category).WithMany(x => x.Topics).HasForeignKey(x => x.CategoryId);
        }

        /// <summary>
        ///     Initialize reply table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddReplyTable(ModelBuilder modelBuilder)
        {
            var reply = modelBuilder.Entity<Reply>();

            // Primary key setting.
            reply.HasKey(x => x.Id);
            reply.Property(x => x.Id).UseSqlServerIdentityColumn();

            // Relationship between reply and topic.
            reply.HasOne(x => x.Topic).WithMany(x => x.Replies).HasForeignKey(x => x.TopicId);
        }

        /// <summary>
        ///     Initialize follow category table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddFollowCategoryTable(ModelBuilder modelBuilder)
        {
            // Find follow category instance.
            var followCategory = modelBuilder.Entity<FollowCategory>();

            // Primary key initialization.
            followCategory.HasKey(x => new {x.FollowerId, x.CategoryId});

            // Relationship between follow category and category.
            followCategory.HasOne(x => x.Category).WithMany(x => x.FollowCategories).HasForeignKey(x => x.CategoryId);
        }

        /// <summary>
        ///     Initialize follow post table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddFollowTopicTable(ModelBuilder modelBuilder)
        {
            // Find follow topic instance.
            var followTopic = modelBuilder.Entity<FollowTopic>();

            // Primary key initialization.
            followTopic.HasKey(x => new {x.FollowerId, x.TopicId});

            // Relationship between follow topic & topic
            followTopic.HasOne(x => x.Topic).WithMany(x => x.FollowTopics).HasForeignKey(x => x.TopicId);
        }

        /// <summary>
        ///     Initialize post report table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddReportTopicTable(ModelBuilder modelBuilder)
        {
            // Find post report instance.
            var topicReport = modelBuilder.Entity<ReportTopic>();

            // Primary key initialization.
            topicReport.HasKey(x => new {x.TopicId, x.ReporterId});
            topicReport.Property(x => x.Reason).IsRequired();

            // Relationship between topic report and topic.
            topicReport.HasOne(x => x.Topic).WithMany(x => x.ReportTopics).HasForeignKey(x => x.TopicId);
        }
        

        /// <summary>
        ///     Add category summary table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddCategorySummaryTable(ModelBuilder modelBuilder)
        {
            var categorySummary = modelBuilder.Entity<CategorySummary>();
            categorySummary.HasKey(x => x.CategoryId);
        }

        /// <summary>
        ///     Add topic summary table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void AddTopicSummaryTable(ModelBuilder modelBuilder)
        {
            var topicSummary = modelBuilder.Entity<TopicSummary>();
            topicSummary.HasKey(x => x.TopicId);

            topicSummary.HasOne(x => x.Topic).WithOne(x => x.TopicSummary).HasForeignKey<TopicSummary>(x => x.TopicId);
        }

        #endregion

        #endregion
    }
}