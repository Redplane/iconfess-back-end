﻿using MainDb.Models.Entities;
using ServiceShared.Interfaces.Services;

namespace MainDb.Interfaces
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        #region Properties

        IBaseRepository<CategoryGroup> CategoryGroups { get; }

        IBaseRepository<Category> Categories { get; }

        IBaseRepository<FollowCategory> FollowingCategories { get; }

        IBaseRepository<Reply> Replies { get; }

        IBaseRepository<Topic> Topics { get; }

        IBaseRepository<FollowTopic> FollowingTopics { get; }

        IBaseRepository<ReportTopic> ReportTopics { get; }
        
        IBaseRepository<CategorySummary> CategorySummaries { get; }

        IBaseRepository<TopicSummary> TopicSummaries { get; }

        #endregion
    }
}